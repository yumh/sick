CC	?= cc
CFLAGS	 = -Wall -O3 -std=c89
LDFLAGS	 =

.PHONY: all clean

all: sick

clean:
	rm -f sick

sick: chunk.c compiler.c scanner.c sick.c sicktable.c sstdlib.c \
		util.c values.c vec.c vm.c
	${CC} ${CFLAGS} chunk.c compiler.c scanner.c sick.c sicktable.c \
		sstdlib.c util.c values.c vec.c vm.c -o sick
