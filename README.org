* Sick

  #+BEGIN_QUOTE
  A simple 'n sick dialect of LISP
  #+END_QUOTE

  Sick lisp aims to be a simple, embeddable lisp dialect:
  - simple means a language without too much frills, something
    straightforward, but also a simple implementation. I'd also like
    it to be portable, hence the choice of C89.
  - embeddable means that should be simple to embed it onto another
    bigger programs, simple to define bindings for it and to script
    complex software.

*** Dependencies
    Make or CMake (to build the project), a C compiler and a ANSI
    standard library.

    I've tested and it works on OpenBSD 6.4 (clang and gcc on amd64),
    Debian 9 (clang and gcc on amd64 and armv8) and android (with
    termux, clang on i386)

*** Building

**** Plain old makefiles
     A makefile is included to ease the building, but it may be a bit
     outdated since I'm using CMake during the
     development. Nevertheless, invoking make should build a =sick=
     executable, but not the tests.

**** CMake
     The usual
     #+BEGIN_SRC sh
       mkdir build
       cd build
       cmake ..
       make
     #+END_SRC or, to make a
       or, for a debug build
     #+BEGIN_SRC sh
       mkdir build
       cd build
       cmake -DCMAKE_BUILD_TYPE=Debug ..
       make
     #+END_SRC

     CMake will, by default, build also the tests, that can be run
     with:
     #+BEGIN_SRC sh
       cd build
       make test
     #+END_SRC

*** Implementation

**** GC
     The garbage collector is a simple stop-the-world mark-and-sweep
     gc, without anything fancy. I'm looking at other types of gc, but
     for the moment this is the only one available.

     The GC is automatically stopped when the vm is created and before
     calling any C-function from sicklisp, and resumed after.

**** VM
     The virtual machine is register-based. It has a dynamic number
     of registers.

**** Compiler
     The compiler is a simple one-pass compiler, it generates
     instructions directly from the output of the scanner. At the
     moment macros are not supported, but they're planned.

*** Acknowledgments
    - The paper /The implementation of Lua 5.0/ for the idea of a register-based
      virtual machine and the one-step compiler.
