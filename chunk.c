#include <assert.h>
#include <stdlib.h>

#include "chunk.h"

void
chunk_init(struct chunk *c, struct vec *cp)
{
	c->cp = cp;

	vec_init(&c->bytecode, sizeof(uint32_t));
}

/* void */
/* chunk_destroy(struct chunk *c) */
/* { */
/* 	vec_deallocate(&c->bytecode); */
/* 	vec_deallocate(&c->cp); */
/* 	free(c); */
/* } */

void
chunk_deallocate(struct chunk *c)
{
	vec_deallocate(&c->bytecode);
}

int
chunk_write(struct chunk *c, uint32_t i)
{
	return vec_push(&c->bytecode, &i);
}

int
chunk_writebuf(struct chunk *c, uint32_t *ii, size_t len)
{
	size_t i;
	for (i = 0; i < len; ++i)
		if (chunk_write(c, ii[i]) == -1)
			return 0;
	return 1;
}

int
chunk_addconst(struct chunk *c, sickval v)
{
	int found, index;

	found = 0;
	index = 0;

	assert(c->cp != NULL);

	VEC_FOREACH(sickval, i, c->cp, {
			if (sickval_equal(v, i))
				return index;
			index++;
		});
	return vec_push(c->cp, &v);
}

sickval *
chunk_constnth(struct chunk *c, int n)
{
	assert(c->cp != NULL);
	return vec_nth(c->cp, n);
}
