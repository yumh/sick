#ifndef CHUNK_H
#define CHUNK_H

#include <stdint.h>

#include "values.h"
#include "vec.h"

struct chunk {
	struct gcobject	gc;
	struct vec	bytecode;
	struct vec	*cp;
};

/* struct chunk	*chunk_new(); */
void	 chunk_init(struct chunk*, struct vec*);
/* void	 chunk_destroy(struct chunk*); */
void	 chunk_deallocate(struct chunk*);
int	 chunk_write(struct chunk*, uint32_t);
int	 chunk_writebuf(struct chunk*, uint32_t*, size_t);
int	 chunk_addconst(struct chunk*, sickval);
sickval	*chunk_constnth(struct chunk*, int);

#endif
