#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "compiler.h"
#include "scanner.h"
#include "vec.h"
#include "util.h"

static inline sicknum
token_to_number(struct token t)
{
	assert(t.type == TOKEN_NUMBER);
	sicknum n = 0, f = 0;
	int decimal = 0, negative = 0;

	if (*t.start == '-' || *t.start == '+') {
		if (*t.start == '-') negative = 1;
		t.start++;
		t.len--;
	}

	for (; t.len > 0; t.start++, t.len--) {
		if (*t.start == '.') {
			decimal = 1;
			continue;
		}

		if (decimal)
			f = f/10 + (sicknum)(*t.start - '0')/10;
		else
			n = n*10 + (*t.start - '0');
	}

	if (negative)
		return -1 * (n+f);
	return n + f;
}

sickval
token_to_sickval(sickvm *vm, struct token t)
{
	switch (t.type) {
	case TOKEN_SYMBOL:
		return sickvm_newsym(vm, t.start, t.len);

	case TOKEN_STRING:
		return sickvm_newstr(vm, t.start, t.len);

	case TOKEN_NUMBER:
		return SICK_NUMBER(token_to_number(t));

	default:
		return SICK_NIL;
	}
}

/* static inline  */

static int compile_thing(struct env*, int, enum token_type, int);

static int
compile_def(struct env *e, int reg)
{
	int max_r, r, i;

	max_r = reg;

	for (i = 0; i < 2; ++i) {
		r = compile_thing(e, reg + i, TOKEN_RIGHT_PAREN, i == 0);
		if (r == COMPILER_ERROR || r == COMPILER_TOKEN_FOUND) {
			return COMPILER_ERROR;
		}
		max_r = MAX(max_r, r);
	}

	/* eat up also the `)' */
	r = compile_thing(e, reg, TOKEN_RIGHT_PAREN, 0);
	if (r != COMPILER_TOKEN_FOUND)
		return COMPILER_ERROR;

	chunk_write(e->f->chunk, SICKOP_DEFINE(reg, reg+1));
	return max_r;
}

struct compiler_tmp_write_defs {
	struct chunk *chunk;	/* the chunk where to write */
	int s_reg; 		/* first free register */
	sickt *v;		/* table with symbols => registers */
};

static void
write_defs(sickval k, sickval v, void *d)
{
	struct compiler_tmp_write_defs *c;
	int off;
	sickval *r;

	c = d;

	if (AS_NUMBER(v) == 0)
		return;

	/* fprintf(stderr, "variable "); */
	/* sickval_print(stderr, k); */
	/* fprintf(stderr, " was used in nested lambdas %f times\n", AS_NUMBER(v)); */

	if ((off = chunk_addconst(c->chunk, k)) == -1)
		abort();
	chunk_write(c->chunk, SICKOP_FETCH(off, c->s_reg));

	r = sickt_get(c->v, k);
	assert(r != NULL);
	chunk_write(c->chunk, SICKOP_DEFINE(c->s_reg, AS_NUMBER(*r)));
}

static int
compile_do(struct env *e, int reg)
{
	int r, t, max_r;

	r = reg;
	t = 0;

	max_r = reg;

	while (1) {
		r = compile_thing(e, reg, TOKEN_RIGHT_PAREN, 0);
		if (r == COMPILER_ERROR)
			return COMPILER_ERROR;
		if (r == COMPILER_TOKEN_FOUND)
			break;
		max_r = MAX(max_r, r);
		++t;
	}

	return max_r;
}

static int
compile_lambda(struct env *e, int reg)
{
	struct chunk tmp, *c;
	struct frame *bk, *fn;
	struct compiler_tmp_write_defs t;
	int r, arity, s_r, off, letlen_bk;

	/* (\ x y z . (....)) */

	s_r = reg;
	arity = 0;

	chunk_init(&tmp, e->f->chunk->cp);

	letlen_bk = e->letlen;
	e->letlen = 0;

	if ((c = sickvm_newchunk_inherit(e->vm, e->f->chunk)) == NULL)
		abort();

	bk = e->f;
	if ((e->f = sickvm_newframe_light(e->vm, &tmp)) == NULL)
		abort();

	if ((e->scope = sickvm_newscope(e->vm, e->scope)) == NULL)
		abort();
	if ((e->wanted = sickvm_newscope(e->vm, e->wanted)) == NULL)
		abort();

	/* read the symbols up to the `.' */
	while (1) {
		struct token t;
		sickval s;

		t = scanner_next_token(e->s);
		if (t.type != TOKEN_SYMBOL)
			return COMPILER_ERROR;

		if (!strncmp(t.start, ".", t.len))
			break;

		s = token_to_sickval(e->vm, t);
		assert(s.type == SV_SYMBOL);

		/* save the register of the var */
		sickt_insert(e->scope->scope, s, SICK_NUMBER(arity));

		/* and save it here for "deep request" */
		sickt_insert(e->wanted->scope, s, SICK_NUMBER(0));

		arity++;
	}

	/* compile the body of the lambda */
	r = compile_do(e, arity);
	if (r == COMPILER_ERROR)
		return COMPILER_ERROR;

	reg = MAX(reg, r);

	/* r = compile_thing(e, reg, TOKEN_RIGHT_PAREN, 0); */
	/* if (r != COMPILER_TOKEN_FOUND) */
	/* 	return COMPILER_ERROR; */

	/* define the variables that are used in deep scopes */
	t.s_reg = reg;
	t.v = e->scope->scope;
	t.chunk = c;
	sickt_do(e->wanted->scope, &write_defs, &t);

	/* pop out the first layer of `scope' and `wanted' */
	e->scope = e->scope->parent;
	e->wanted = e->wanted->parent;

	/* copy the tmp to the "true" chunk */
	e->f->chunk = c;
	chunk_writebuf(c, tmp.bytecode.items, tmp.bytecode.count);
	chunk_write(c, SICKOP_RET(arity));

	/* deallocate tmp */
	chunk_deallocate(&tmp);

	/* create the function */
	fn = e->f;
	e->f = bk;
	/* set the correct number of registers */
	vec_resize(fn->reg, reg);
	if ((off = chunk_addconst(e->f->chunk, sickvm_newfn(e->vm, fn, arity))) == -1)
		abort();
	chunk_write(e->f->chunk, SICKOP_FINSCOPE(off, s_r));

	e->letlen = letlen_bk;

	return ++reg;
}

static int
compile_recur(struct env *e, int reg)
{
	/*
	 * The idea is that if we're in a lambda, the current chunk
	 * contain only the function' code, so we get the len, COPY
	 * the registers at their place and JMP to the start of the
	 * chunk.
	 */

	int32_t jmp;
	int max_r, i, j, k;

	i = max_r = reg;

	/* TODO: check that we are in a lambda */
	/* TODO: check the correct arity */

	while (1) {
		int r;

		r = compile_thing(e, i, TOKEN_RIGHT_PAREN, 0);
		max_r = MAX(r, max_r);

		if (r == COMPILER_ERROR)
			return COMPILER_ERROR;
		if (r == COMPILER_TOKEN_FOUND)
			break;

		++i;
	}

	for (j = reg, k = 0; j < i; ++j, ++k)
		chunk_write(e->f->chunk, SICKOP_COPY(j, k));

	jmp = -1 * (e->f->chunk->bytecode.count + e->letlen + 1);
	if (jmp < INT16_MIN) {
		fprintf(stderr, "JMP too high (%d), cannot be done\n", jmp);
		abort();
	}
	chunk_write(e->f->chunk, SICKOP_JMP(jmp));

	return MAX(reg, max_r);
}

static int
compile_if(struct env *e, int reg)
{
#define check_status(r)				\
	if (r == COMPILER_ERROR)		\
		return COMPILER_ERROR;		\
	if (r == COMPILER_TOKEN_FOUND)		\
		return COMPILER_ERROR;		\

	uint32_t jmp;
	size_t f_jmp, s_jmp, end;
	struct token t;
	int r, max_r;

	r = compile_thing(e, reg, TOKEN_RIGHT_PAREN, 0);
	check_status(r);

	chunk_write(e->f->chunk, SICKOP_TEST(reg));
	f_jmp = chunk_write(e->f->chunk, SICKOP_JMP(0)); /* will be fixed later! */

	/* compile the first form */
	r = compile_thing(e, reg, TOKEN_RIGHT_PAREN, 0);
	check_status(r);
	max_r = MAX(r, reg);
	s_jmp = chunk_write(e->f->chunk, SICKOP_JMP(0)); /* will be fixed later! */

	jmp = s_jmp - f_jmp;
	/* fix the first JMP */
	if (jmp > INT16_MAX) {
		fprintf(stderr, "JMP too high (%zu), cannot be done\n", (size_t)jmp);
		abort();
	}
	vec_set(&e->f->chunk->bytecode, f_jmp, &SICKOP_JMP(jmp));

	/* compile the second form */
	r = compile_thing(e, reg, TOKEN_RIGHT_PAREN, 0);
	check_status(r);
	max_r = MAX(r, reg);

	t = scanner_next_token(e->s);
	if (t.type != TOKEN_RIGHT_PAREN) {
		fprintf(stderr, "if should have only one `else' clause.\n");
		return COMPILER_ERROR;
	}

	/* get the end addr */
	end = e->f->chunk->bytecode.count;

	jmp = end - s_jmp - 1;
	if (jmp > INT16_MAX) {
		fprintf(stderr, "JMP too high (%d), cannot be done\n", jmp);
		abort();
	}
	vec_set(&e->f->chunk->bytecode, s_jmp, &SICKOP_JMP(jmp));

#undef check_status
	return max_r;
}

static int
compile_let(struct env *e, int reg)
{
	struct token t;
	struct chunk *bk, newc;
	struct compiler_tmp_write_defs d;
	struct vec vs;
	int r, max_r, base_r, letlen_bk;

	/* (let {x 1} ...) */

	base_r = reg;

	if (e->scope == NULL)
		return COMPILER_ERROR;

	letlen_bk = e->letlen;
	e->letlen += e->f->chunk->bytecode.count;

	max_r = reg;

	vec_init(&vs, sizeof(sickval));

	assert(e->f->chunk->cp != NULL);
	chunk_init(&newc, e->f->chunk->cp);
	bk = e->f->chunk;
	e->f->chunk = &newc;

	t = scanner_next_token(e->s);
	if (t.type != TOKEN_LEFT_BRACE)
		return COMPILER_ERROR;

	while (1) {
		sickval s;

		/* read the symbol */
		t = scanner_next_token(e->s);
		if (t.type == TOKEN_RIGHT_BRACE) /* we're done! */
			break;

		if (t.type != TOKEN_SYMBOL)
			goto err;

		s = token_to_sickval(e->vm, t);
		if (sickt_has(e->scope->scope, s))
			goto err;
		vec_push(&vs, &s);

		if (e->scope != NULL) {
			sickt_insert(e->scope->scope, s, SICK_NUMBER(reg));
			sickt_insert(e->wanted->scope, s, SICK_NUMBER(0));
		}

		/* compile the value */
		r = compile_thing(e, reg, TOKEN_RIGHT_BRACE, 0);
		if (r == COMPILER_ERROR)
			goto err;
		if (r == COMPILER_TOKEN_FOUND)
			goto err;
		max_r = MAX(r, max_r);

		++reg;
	}
	max_r = MAX(max_r, reg);

	/* compile the body of the let */
	r = compile_do(e, reg);
	if (r == COMPILER_ERROR)
		goto err;
	max_r = MAX(r, max_r);

	/* define the vars that needs to be defined */
	d.s_reg = reg;
	d.v = e->scope->scope;
	d.chunk = bk;
	sickt_do(e->wanted->scope, &write_defs, &d);

	chunk_writebuf(bk, newc.bytecode.items, newc.bytecode.count);
	chunk_write(bk, SICKOP_COPY(reg, base_r));
	e->f->chunk = bk;

	VEC_FOREACH(sickval, i, &vs, {
			assert(i.type == SV_SYMBOL);
			sickt_insert(e->wanted->scope, i, SICK_NUMBER(0));
		});

	e->letlen = letlen_bk;

	chunk_deallocate(&newc);
	vec_deallocate(&vs);
	return max_r;

err:
	chunk_deallocate(&newc);
	vec_deallocate(&vs);
	return COMPILER_ERROR;
}

static int
inline_math_op(struct env *e, int reg, enum sickop t)
{
	int max_r, r, i;
	sickop op;

	max_r = reg;

	r = compile_thing(e, reg, TOKEN_RIGHT_PAREN, 0);
	reg++;
	if (r == COMPILER_ERROR)
		return r;

	/* 0 arity case */
	if (r == COMPILER_TOKEN_FOUND)
		return COMPILER_ERROR;

	max_r = MAX(r, max_r);

	i = 1;
	while (1) {
		r = compile_thing(e, reg, TOKEN_RIGHT_PAREN, 0);
		reg++;
		if (r == COMPILER_ERROR)
			return r;
		if (r == COMPILER_TOKEN_FOUND)
			break;
		max_r = MAX(r, max_r);

		++i;

		if (i % 2 == 0) {
			op.u.i = t;
			op.u.op1 = reg - 2;
			op.u.op2 = reg - 1;
			op.u.op3 = reg - 2;
			chunk_write(e->f->chunk, op.raw);

			reg -= 2;
		}
	}

	/* 1 arity case */
	if (i == 1)
		return COMPILER_ERROR;

	return max_r;
}

static int
should_inline_function(struct env *e, int reg, struct token t)
{
	if (!strncmp(t.start, "+", t.len))
		return inline_math_op(e, reg, OP_ADD);
	if (!strncmp(t.start, "-", t.len))
		return inline_math_op(e, reg, OP_SUB);
	if (!strncmp(t.start, "*", t.len))
		return inline_math_op(e, reg, OP_MUL);
	if (!strncmp(t.start, "/", t.len))
		return inline_math_op(e, reg, OP_DIV);
	/* if (!strncmp(t.start, "=", t.len)) */
	/* 	return inline_eq(e, reg); */
	/* if (!strncmp(t.start, "not", t.len)) */
	/* 	return inline_not(e, reg); */

	return COMPILER_CANNOT_INLINE;
}

static int
compile_funcall(struct env *e, int reg)
{
	int r, base_r, args;
	struct token t;

	args = reg -1;
	base_r = reg;

	/* Check if the first `thing' is special */
	t = scanner_next_token(e->s);
	if (!strncmp(t.start, "def", t.len))
		return compile_def(e, reg);
	if (!strncmp(t.start, "\\", t.len))
		return compile_lambda(e, reg);
	if (!strncmp(t.start, "recur", t.len))
		return compile_recur(e, reg);
	if (!strncmp(t.start, "do", t.len))
		return compile_do(e, reg);
	if (!strncmp(t.start, "if", t.len))
		return compile_if(e, reg);
	if (!strncmp(t.start, "let", t.len))
		return compile_let(e, reg);

	/* check if we should inline the function */
	if ((r = should_inline_function(e, reg, t)) != COMPILER_CANNOT_INLINE)
		return r;

	/* otherwise unwind the scanner and proceed */
	scanner_unwind(e->s, t);

	while (1) {
		r = compile_thing(e, args + 1, TOKEN_RIGHT_PAREN, 0);
		reg = MAX(r, reg);

		if (r == COMPILER_ERROR)
			return COMPILER_ERROR;

		if (r == COMPILER_TOKEN_FOUND)
			break;
		args++;
	}
	args = MAX(args, base_r);

	/* chunk_write(e->f->chunk, SICKOP_PREPCALL(args - base_r)); */
	for (r = base_r+1; r <= args; ++r)
		chunk_write(e->f->chunk, SICKOP_PUSH(r));
	chunk_write(e->f->chunk, SICKOP_CALL(base_r, base_r));

	return reg;
}

static int
compile_vec(struct env *e, int reg)
{
	int off, base_r, r, args;

	base_r = reg;
	args = reg;

	if ((off = chunk_addconst(e->f->chunk, sickvm_newsym(e->vm, "vec", -1))) == -1) {
		fprintf(stderr, "Cannot push token in CP.\n");
		abort();
	}
	chunk_write(e->f->chunk, SICKOP_LOAD(off, base_r));
	args++;

	while (1) {
		r = compile_thing(e, args, TOKEN_RIGHT_BRACKET, 0);
		reg = MAX(r, reg);

		if (r == COMPILER_ERROR)
			return COMPILER_ERROR;

		if (r == COMPILER_TOKEN_FOUND)
			break;

		args++;
	}

	/* chunk_write(e->f->chunk, SICKOP_PREPCALL(args - base_r - 1)); */
	for (r = base_r+1; r < args; ++r)
		chunk_write(e->f->chunk, SICKOP_PUSH(r));
	chunk_write(e->f->chunk, SICKOP_CALL(base_r, base_r));
	return MAX(reg, args);
}

static int
compile_map(struct env *e, int reg)
{
	int off, base_r, args, r;

	base_r = reg;
	args = reg;

	if ((off = chunk_addconst(e->f->chunk, sickvm_newsym(e->vm, "map", -1))) == -1) {
		fprintf(stderr, "Cannot push token in CP.\n");
		abort();
	}
	chunk_write(e->f->chunk, SICKOP_LOAD(off, base_r));
	args++;

	while (1) {
		r = compile_thing(e, args, TOKEN_RIGHT_BRACE, 0);
		reg = MAX(r, reg);

		if (r == COMPILER_ERROR)
			return COMPILER_ERROR;
		if (r == COMPILER_TOKEN_FOUND)
			break;

		args++;
	}

	/* args MUST be odd, 'causo it's `map' plus the (even) number of elements */
	if ((args-base_r) % 2 == 0) {
		fprintf(stderr, "a map should have a even number of elements\n");
		return COMPILER_ERROR;
	}

	/* chunk_write(e->f->chunk, SICKOP_PREPCALL(args - base_r -1)); */
	for (r = base_r + 1; r < args; ++r)
		chunk_write(e->f->chunk, SICKOP_PUSH(r));
	chunk_write(e->f->chunk, SICKOP_CALL(base_r, base_r));
	return MAX(reg, args);
}

static inline void
find_and_inc(struct env *e, sickval v)
{
	struct scope *w;
	sickval *n;

	assert(v.type == SV_SYMBOL);

	if (e->wanted == NULL)
		return;

	/* skip the first level of scope */
	w = e->wanted->parent;

	while (w != NULL) {
		n = sickt_get(w->scope, v);
		if (n != NULL) {
			assert(n->type == SV_NUMBER);
			n->as.number++;
			break;
		}

		w = w->parent;
	}
}

static inline int
load_symbol(struct env *e, sickval s, int reg)
{
	sickval	*reg_no;
	int off;

	assert(s.type == SV_SYMBOL);

	if (e->scope != NULL) {
		reg_no = sickt_get(e->scope->scope, s);
		if (reg_no != NULL) {
			/* FOUND!  */
			/* TODO: we should 1) push it or 2) RETurning it, copying is a placeholder for now */
			chunk_write(e->f->chunk, SICKOP_COPY(AS_NUMBER(*reg_no), reg));
			return ++reg;
		}
	}

	/* otherwise we have to LOAD it */

	/* but don't forget to notify that we're using it! */
	find_and_inc(e, s);

	if ((off = chunk_addconst(e->f->chunk, s)) == -1) {
		fprintf(stderr, "Cannot push token in CP.\n");
		abort();
	}
	chunk_write(e->f->chunk, SICKOP_LOAD(off, reg));
	return ++reg;
}

static int
compile_thing(struct env *e, int reg, enum token_type stopt, int is_quoted)
{
	struct token t;

	t = scanner_next_token(e->s);

	if (t.type == stopt)
		return COMPILER_TOKEN_FOUND;

	switch (t.type) {
	case TOKEN_RIGHT_BRACE:
	case TOKEN_RIGHT_BRACKET:
	case TOKEN_RIGHT_PAREN:
	case TOKEN_HAT:
	case TOKEN_APOSTROPHE:
	case TOKEN_UNTERMINATED_STRING:
	case TOKEN_HASH:
	case TOKEN_EOF:
		fprintf(stderr, "Don't know how to handle the token (%d)\n", t.type);
		return COMPILER_ERROR;

	case TOKEN_COLON:
		if (is_quoted) /* double (or more) quoting */
			return COMPILER_ERROR;
		return compile_thing(e, reg, stopt, 1);

	case TOKEN_LEFT_PAREN:
		/* if (is_quoted) */
		/* 	return compile_vec(vm, s, c, err, reg); */
		/* else */
		return compile_funcall(e, reg);

	case TOKEN_LEFT_BRACKET:
		return compile_vec(e, reg);

	case TOKEN_LEFT_BRACE:
		return compile_map(e, reg);

	case TOKEN_STRING:
	case TOKEN_SYMBOL:
	case TOKEN_NUMBER: {
		sickval v;
		int off;

		v = token_to_sickval(e->vm, t);

		if (v.type == SV_SYMBOL && !is_quoted)
			return load_symbol(e, v, reg);

		if ((off = chunk_addconst(e->f->chunk, v)) == -1) {
			fprintf(stderr, "Cannot push token in CP.\n");
			abort();
		}

		chunk_write(e->f->chunk, SICKOP_FETCH(off, reg));
		return ++reg;
	}
	}

	/* if, for whatever reason, we get here, something is definitely wrong! */
	return COMPILER_ERROR;
}

static inline struct frame *
prepare_frame(sickvm *vm)
{
	struct chunk *c;
	struct frame *parent;
	sickval *parent_frame;

	if ((c = sickvm_newchunk(vm)) == NULL)
		return NULL;

	parent_frame = sickt_get(vm->namespaces, sickvm_newsym(vm, "sick.core", -1));
	parent = parent_frame == NULL ? NULL : AS_FRAME(*parent_frame);

	return sickvm_newframe(vm, parent, c);
}

struct frame *
compile(sickvm *vm, const char *str, ssize_t len)
{
	struct frame *f;
	struct scanner s;
	struct env env;
	int r, regs;

	regs = 0;

	if (len < 0)
		len = strlen(str);

	if ((f = prepare_frame(vm)) == NULL)
		return NULL;

	s = scanner_new(str, len);

	env = (struct env){
		.vm = vm,
		.s = &s,
		.f = f,
		.letlen = 0,
		.scope = NULL,
		.wanted = NULL
	};

	while (1) {
		r = compile_thing(&env, 0, TOKEN_EOF, 0);

		if (r == COMPILER_ERROR) {
			scanner_printerror(stderr, &s, s.last_token);
			return NULL;
		}

		if (r == COMPILER_TOKEN_FOUND)
			break;

		regs = MAX(regs, r);
	}

	vec_resize(f->reg, regs);

	return f;
}

int
compile_and_run(sickvm *vm, const char *str, ssize_t len, sickval *ret)
{
	struct frame *f;

	/* The GC could collect things while we're creating 'em */
	sickvm_gc_pause(vm);

	if ((f = compile(vm, str, len)) == NULL)
		return COMPILER_ERROR;

	*ret = sickvm_push(vm, f);
	return 1;
}

int
compile_and_eval_in_ns(sickvm *vm, const char *str, ssize_t len, sickval ns, sickval *ret)
{
	struct frame *f;

	f = prepare_frame(vm);

	if ((f = compile(vm, str, len)) == NULL)
		return 0;

	*ret = sickvm_eval_in_ns(vm, f, ns);
	return 1;
}

static void
print_frame(struct frame *fp, FILE *f, int offset)
{
	int j, k;

	j = 0;
	VEC_FOREACH(sickop, i, &fp->chunk->bytecode, {
			for (k = 0; k < offset; ++k)
				fputc('\t', f);
			compile_dump_instruction(fp->chunk, i, f);
		});

	fputc('\n', f);
	if (offset != 0)
		return;

	for (k = 0; k < offset; ++k)
		fputc('\t', f);
	fprintf(f, "CP:\n");

	j = 0;
	VEC_FOREACH(sickval, i, fp->chunk->cp, {
			for (k = 0; k < offset; ++k)
				fputc('\t', f);
			fprintf(f, " %d) ", j++);
			if (i.type == SV_FUN && !AS_FUN(i)->cfn) {
				fprintf(f, "ariety: %d, reg_no: %zu\n", AS_FUN(i)->fn.nativefn.arity, vec_cap(AS_FUN(i)->fn.nativefn.frame->reg));
				/* for (k = 0; k < offset; ++k) */
				/* 	fputc(' ', f); */
				print_frame(AS_FUN(i)->fn.nativefn.frame, f, offset+1);
			} else
				sickval_print(f, i);
			fprintf(f, "\n");
		});
}

void
compile_dump_bytecode(sickvm *vm, const char *src, ssize_t len, FILE *f)
{
	struct frame *fp;

	if ((fp = compile(vm, src, len)) == NULL) {
		fprintf(f, "Cannot compile chunk\n");
		return;
	}

	print_frame(fp, f, 0);
}

void
compile_dump_instruction(struct chunk *c, sickop i, FILE *f)
{
	switch(i.u.i) {
	case OP_DEFINE:
		fprintf(f, "define\t r%d r%d", i.u.op1, i.u.op2);
		break;

	case OP_FETCH:
		if (i.u.op3)
			fprintf(f, "finscope ");
		else
			fprintf(f, "fetch\t ");
		fprintf(f, "$%d r%d \t;; ", i.u.op1, i.u.op2);
		sickval_print(f, *chunk_constnth(c, i.u.op1));
		break;

	case OP_LOAD:
		fprintf(f, "load\t $%d r%d \t;; ", i.u.op1, i.u.op2);
		sickval_print(f, *chunk_constnth(c, i.u.op1));
		break;

	/* case OP_PREPCALL: */
	/* 	fprintf(f, "prepcall %d", i.lu.op1); */
	/* 	break; */

	case OP_PUSH:
		fprintf(f, "push\t r%d", i.u.op1);
		break;

	case OP_CALL:
		fprintf(f, "call\t r%d r%d", i.u.op1, i.u.op2);
		break;

	case OP_COPY:
		fprintf(f, "copy\t r%d r%d", i.u.op1, i.u.op2);
		break;

	case OP_RET:
		fprintf(f, "ret\t r%d", i.u.op1);
		break;

	case OP_JMP:
		fprintf(f, "jmp\t $%d", i.ls.op1);
		break;

	case OP_TEST:
		fprintf(f, "test\t r%d", i.u.op1);
		break;

	case OP_ADD:
		fprintf(f, "add\t r%d r%d r%d", i.u.op1, i.u.op2, i.u.op3);
		break;
	case OP_SUB:
		fprintf(f, "sub\t r%d r%d r%d", i.u.op1, i.u.op2, i.u.op3);
		break;
	case OP_MUL:
		fprintf(f, "mul\t r%d r%d r%d", i.u.op1, i.u.op2, i.u.op3);
		break;
	case OP_DIV:
		fprintf(f, "div\t r%d r%d r%d", i.u.op1, i.u.op2, i.u.op3);
		break;

	}
	fprintf(f, "\n");
}
