#ifndef COMPILER_H
#define COMPILER_H

#include <stdio.h>

#include "vm.h"
#include "scanner.h"

#define COMPILER_ERROR -1
#define COMPILER_TOKEN_FOUND -2
#define COMPILER_CANNOT_INLINE -3

struct env {
	sickvm		*vm;
	struct scanner	*s;
	struct frame	*f;

	int		letlen;

	struct scope	*scope;  /* symbol => register */
	struct scope	*wanted; /* symbol => integer */
};

sickval		 token_to_sickval(sickvm*, struct token);
struct frame	*compile(sickvm*, const char*, ssize_t);
int		 compile_and_run(sickvm*, const char*, ssize_t, sickval*);
int		 compile_and_eval_in_ns(sickvm*, const char*, ssize_t, sickval, sickval*);

void		 compile_dump_bytecode(sickvm*, const char*, ssize_t, FILE*);
void		 compile_dump_instruction(struct chunk*, sickop, FILE*);

#endif
