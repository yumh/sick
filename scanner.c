#include <assert.h>
#include <limits.h>
#include <stdlib.h>

#include "scanner.h"
#include "util.h"

struct scanner
scanner_new(const char *c, size_t len)
{
	struct scanner s = {
		.start = c,
		.current = c,
		.len = len,
		.line = 1,
		.last_token = {
			.type = -1,
			.start = c,
			.len = 0,
			.line = 0,
		}
	};
	return s;
}


const char *
scanner_token_str(enum token_type t)
{
	switch (t) {
	case TOKEN_LEFT_PAREN: return "left_paren";
	case TOKEN_RIGHT_PAREN: return "right_paren";
	case TOKEN_LEFT_BRACKET: return "left_bracket";
	case TOKEN_RIGHT_BRACKET: return "right_bracket";
	case TOKEN_LEFT_BRACE: return "left_brace";
	case TOKEN_RIGHT_BRACE: return "right_brace";
	case TOKEN_HAT: return "hat";
	case TOKEN_COLON: return "colon";
	case TOKEN_APOSTROPHE: return "apostrophe";
	case TOKEN_HASH: return "hash";
	case TOKEN_SYMBOL: return "symbol";
	case TOKEN_STRING: return "string";
	case TOKEN_NUMBER: return "number";
	case TOKEN_EOF: return "EOF";
	case TOKEN_UNTERMINATED_STRING: return "unterminated string";
	default:
		abort();
	}
}

int
scanner_eof(struct scanner *s)
{
	return s->current >= s->start + s->len;
}

static inline void
skip_whitespaces(struct scanner *s)
{
	for (;;) {
		if (scanner_eof(s))
			return;

		switch (*s->current) {
		case ';':
			while (*s->current != '\n' && !scanner_eof(s))
				s->current++;
			/* fallthrough */
		case '\n':
			s->line++;
			/* fallthrough */
		case ' ':
		case '\r':
		case '\t':
			s->current++;
			break;

		default: return;
		}
	}
}

static inline struct token
make_symbol(struct scanner *s)
{
	const char	*start;
	char		curr;

	start = s->current;

	for (;;) {
		if (scanner_eof(s))
			break;
		curr = *s->current;
		if (   curr == ' '
		    || curr == '\t'
		    || curr == '\n'
		    || curr == '\r'
		    || curr == '('
		    || curr == ')'
		    || curr == '['
		    || curr == ']'
		    || curr == '{'
		    || curr == '}')
			break;
		s->current++;
	}

	return TOKEN_NEW(TOKEN_SYMBOL, start, s->current - start, s->line);
}

static inline struct token
make_string(struct scanner *s)
{
	const char	*start;
	char		curr;
	size_t		line;
	int		escape;

	/* skip the opening \" */
	assert(*s->current == '\"');
	s->current++;

	line = s->line;
	start = s->current;
	escape = 0;

	for (;;) {
		if (scanner_eof(s))
			return TOKEN_NEW(TOKEN_UNTERMINATED_STRING, start, 1, line);

		curr = *s->current++;
		if (curr == '\\' && !escape) {
			escape = 1;
			continue;
		}

		if (curr == '\"' && !escape)
			break; 	/* we're done! */

		if (curr == '\n')
			s->line++;

		if (escape)
			escape = 0;
	}

	return TOKEN_NEW(TOKEN_STRING, start, s->current - start -1, line);
}

static inline int
can_be_digit(struct scanner *s)
{
	return *s->current >= '0' && *s->current <= '9';
}

static inline int
could_be_part_of_a_number(char c)
{
	return (c >= '0' && c <= '9')
		|| c == 'x'
		|| c == 'b'
		|| c == '.'
		|| (c >= 'A' && c <= 'F')
		|| (c >= 'a' && c <= 'f');
}

static inline struct token
make_number(struct scanner *s)
{
	const char	*start;

	start = s->current;

	for (;;) {
		if (scanner_eof(s))
			break;

		if (!could_be_part_of_a_number(*s->current))
			break;

		s->current++;
	}

	return TOKEN_NEW(TOKEN_NUMBER, start, s->current - start, s->line);
}

static inline struct token
next_token(struct scanner *s)
{
	skip_whitespaces(s);

	if (scanner_eof(s))
		return TOKEN_SIMPLE(TOKEN_EOF, s);

	if (can_be_digit(s))
		return make_number(s);

	switch (*s->current) {
	case '(': TOKEN_RETURN_SIMPLE_INCR(TOKEN_LEFT_PAREN, s);
	case ')': TOKEN_RETURN_SIMPLE_INCR(TOKEN_RIGHT_PAREN, s);
	case '[': TOKEN_RETURN_SIMPLE_INCR(TOKEN_LEFT_BRACKET, s);
	case ']': TOKEN_RETURN_SIMPLE_INCR(TOKEN_RIGHT_BRACKET, s);
	case '{': TOKEN_RETURN_SIMPLE_INCR(TOKEN_LEFT_BRACE, s);
	case '}': TOKEN_RETURN_SIMPLE_INCR(TOKEN_RIGHT_BRACE, s);
	case '^': TOKEN_RETURN_SIMPLE_INCR(TOKEN_HAT, s);
	case ':': TOKEN_RETURN_SIMPLE_INCR(TOKEN_COLON, s);
	case '#': TOKEN_RETURN_SIMPLE_INCR(TOKEN_HASH, s);
	case '\'': TOKEN_RETURN_SIMPLE_INCR(TOKEN_APOSTROPHE, s);
	case '\"': return make_string(s);
	}

	return make_symbol(s);
}

struct token
scanner_next_token(struct scanner *s)
{
	return s->last_token = next_token(s);
}

void
scanner_unwind(struct scanner *s, struct token t)
{
	if (s->current == s->start)
		return;
	s->current = t.start;
	s->line = t.line;
}

void
scanner_dump(FILE *out, struct token t)
{
	fprintf(out, "%s", scanner_token_str(t.type));
	switch (t.type) {
	case TOKEN_SYMBOL:
		fprintf(out, " '%.*s", (int)t.len, t.start);
		break;

	case TOKEN_STRING:
		fprintf(out, " \"%.*s\"", (int)t.len, t.start);
		break;

	case TOKEN_NUMBER:
		fprintf(out, " %.*s", (int)t.len, t.start);
		break;

	default: break;
	}

	fprintf(out, "\n");
}

void
scanner_dumpall(FILE *out, const char *in, size_t len)
{
	size_t line = -1;
	struct token t;
	struct scanner s = scanner_new(in, len);

	do {
		t = scanner_next_token(&s);
		if (line != t.line) {
			fprintf(out, "%3zu ", t.line);
			line = t.line;
		} else
			fprintf(out, "  | ");

		scanner_dump(out, t);
	} while (t.type != TOKEN_EOF);
}

void
scanner_printerror(FILE *f, struct scanner *s, struct token t)
{
	const char *linestart, *lineend;
	ssize_t len, pos;

	/* Find the start of the line */
	linestart = t.start;
	while (linestart > s->start)
		if (*linestart != '\n')
			linestart--;
		else
			break;
	/* skip the newline of the line before */
	if (*linestart == '\n')
		linestart++;
	/* find the end of the line (or the file) */
	lineend = t.start;
	while (lineend < s->start + s->len)
		if (*lineend != '\n')
			lineend++;
		else
			break;

	len = MIN(lineend - linestart, INT_MAX);
	fprintf(f, "%.*s ; line %zu\n", (int)len, linestart, t.line);

	pos = t.start - linestart - 1;
	while (pos > 0) {
		fputc(' ', f);
		pos--;
	}
	fprintf(f, "^\n");
}
