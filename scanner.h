#ifndef SCANNER_H
#define SCANNER_H

#include <stddef.h>
#include <stdio.h>

#define TOKEN_NEW(t, s, l, ll) ((struct token){.type = t, .start = s, .len = l, .line = ll})
#define TOKEN_SIMPLE(t, s) TOKEN_NEW(t, s->current, 1, s->line)
#define TOKEN_RETURN_SIMPLE_INCR(t, s)				\
	do {							\
		s->current++;					\
		return TOKEN_NEW(t, s->current, 1, s->line);	\
	} while (0)

enum token_type {
	/* single char tokens */
	TOKEN_LEFT_PAREN, TOKEN_RIGHT_PAREN, TOKEN_LEFT_BRACKET,
	TOKEN_RIGHT_BRACKET, TOKEN_LEFT_BRACE, TOKEN_RIGHT_BRACE,
	TOKEN_HAT, TOKEN_COLON, TOKEN_APOSTROPHE, TOKEN_HASH,

	/* slightly more complex tokens */
	TOKEN_SYMBOL, TOKEN_STRING, TOKEN_NUMBER,

	/* status/error */
	TOKEN_EOF, TOKEN_UNTERMINATED_STRING
};

struct token {
	enum token_type	type;
	const char	*start;
	size_t		len;
	size_t		line;
};

struct scanner {
	const char	*start;
	const char	*current;
	size_t		len;
	size_t		line;
	struct token	last_token;
};

struct scanner	 scanner_new(const char*, size_t);

const char	*scanner_token_str(enum token_type);

int		 scanner_eof(struct scanner*);
struct token	 scanner_next_token(struct scanner*);
void		 scanner_unwind(struct scanner*, struct token);

void		 scanner_dump(FILE*, struct token);
void		 scanner_dumpall(FILE*, const char*, size_t);

void		 scanner_printerror(FILE*, struct scanner*, struct token);

#endif
