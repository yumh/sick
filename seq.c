#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

/* On OpenBSD there is jot, on GNU/linux seq, ... */
/* I only need a small utility to generate tests, so this should be enough */

int
main(int argc, char **argv)
{
	long n;
	char *c;

	if (argc < 2) {
		fprintf(stderr, "USAGE: %s n\n", argv[0]);
		return 1;
	}
	
	errno = 0;
	n = strtol(argv[1], &c, 10);
	if (*argv[0] == '\0' || *c != '\0') {
		fprintf(stderr, "%s is not a number\n", argv[1]);
		return 2;
	}
	if (errno == ERANGE && (n == LONG_MAX || n == LONG_MIN)) {
		fprintf(stderr, "%s is out of range for a long\n", argv[1]);
		return 3;
	}

	while (n > 0)
		printf("%ld\n", n--);

	return 0;
}
