#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "vm.h"
#include "compiler.h"
#include "scanner.h"
#include "values.h"
#include "sstdlib.h"
#include "util.h"

enum mode {
	RUN, DUMP_SCANNER, DUMP_BYTECODE,
};

sickval
my_exit(sickvm *vm, struct vec *v)
{
	UNUSED(v);

	sickvm_destroy(vm);
	exit(0);

	return SICK_NIL;
}

void
process_file(sickvm *vm, FILE *f, enum mode m)
{
	char *s;
	long size;
	sickval ret;

	fseek(f, 0, SEEK_END);
	size = ftell(f);
	fseek(f, 0, SEEK_SET);

	s = calloc(size + 1, 1);
	fread(s, 1, size, f);

	if (m == DUMP_SCANNER)
		scanner_dumpall(stdout, s, size);
	else if (m == DUMP_BYTECODE)
		compile_dump_bytecode(vm, s, size, stdout);
	else
		compile_and_run(vm, s, size, &ret);

	free(s);
}

void
usage(const char *prgname)
{
	printf("USAGE: %s [-h] [-d] [-r] [file]\n", prgname);
	printf(" -d :: enable scanner dump (no evaluation)\n");
	printf(" -d :: dump the bytecode\n");
}

void
print_prompt()
{
	/* TODO: portable way to do isatty(3) ? */
	printf("> ");
	fflush(stdout);
}

int
main(int argc, char **argv)
{
	sickvm *vm;
	int i;
	enum mode m;
	FILE *f = stdin;
	sickval *parent_frame, ret;

	vm = NULL;
	m = RUN;
	for (i = 1; i < argc; ++i) {
		if (!strcmp(argv[i], "-d")) {
			m = DUMP_SCANNER;
			continue;
		}

		if (!strcmp(argv[i], "-b")) {
			m = DUMP_BYTECODE;
			continue;
		}

		if (!strcmp(argv[i], "-h")) {
			usage(argv[0]);
			return 0;
		}

		if (f != stdin)
			fclose(f);

		f = fopen(argv[i], "r");
		if (f == NULL) {
			perror("Cannot open file");
			return 1;
		}
	}

	if ((vm = sickvm_new()) == NULL)
		abort();

	parent_frame = sickt_get(vm->namespaces, sickvm_newsym(vm, "sick.core", -1));
	if (parent_frame == NULL)
		abort();
	sickt_insert(AS_FRAME(*parent_frame)->scope->scope, sickvm_newsym(vm, "exit", -1), sickvm_newcfun(vm, my_exit));

	if (f != stdin) {
		process_file(vm, f, m);
		fclose(f);
		goto exit;
	}

	compile_and_eval_in_ns(
		vm,
		"(def repl"
		"  (\\ ."
		"    (print \"> \")"
		"    (flush)"
		"    (let {text (read)}"
		"      (if (= text \"exit\")"
		"          nil"
		"          (do "
		"              (println (eval text))"
		"              (recur))))))"
		"(repl)",
		-1,
		sickvm_newsym(vm, "user", -1),
		&ret);

exit:
	sickvm_destroy(vm);

	return 0;
}
