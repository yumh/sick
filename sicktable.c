#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sicktable.h"
#include "util.h"
#include "sllimits.h"
#include "debug.h"

sickt *
sickt_new()
{
	sickt *t;

	if ((t = calloc(1, sizeof(sickt))) == NULL)
		return NULL;

	if (sickt_init(t))
		return t;

	free(t);
	return NULL;
}

int
sickt_init(sickt *t)
{
	t->size = SICKTABLE_INITIAL_SIZE;
	if ((t->items = calloc(t->size, sizeof(struct sickti))) == NULL)
		return 0;

	return 1;
}

void
sickt_deallocate(sickt *t)
{
	size_t		i;
	struct sickti	*e, *f;

	for (i = 0; i < t->size; ++i) {
		e = t->items[i];
		while (e != NULL) {
			f = e->next;
			free(e);
			e = f;
		}
	}

	free(t->items);
}

void
sickt_destroy(sickt *t)
{
	sickt_deallocate(t);
	free(t);
}

/* Bernstein's hash (customized) */
size_t
sickt_hash(const char *s, ssize_t len)
{
	unsigned long h;

	if (len == -1)
		len = strlen(s);

	len = MIN(len, SICKTABLE_MAX_ACCESS_RESIZE);

	h = 7;
	for (; len != 0; ++s, --len)
		h = ((h << 5) + h) + *s;
	return h;
}

static inline void
insert_sickti(struct sickti **v, size_t len, struct sickti *i, size_t h)
{
	i->next = v[h % len];
	v[h % len] = i;
}

static int
sickt_resize(sickt *t)
{
	struct sickti **v;
	struct sickti *i, *tmp;
	size_t a, newsize;

	newsize = t->size * SICKTABLE_RESIZE_FACTOR;
	v = calloc(newsize, sizeof(struct sickti));
	if (v == NULL) {
		free(v);
		return 0;
	}

	for (a = 0; a < t->size; ++a) {
		i = t->items[a];
		while (i != NULL){
			tmp = i->next;
			i->next = NULL;
			insert_sickti(v, newsize, i, AS_STRING(i->key)->hash);
			i = tmp;
		}
	}
	free(t->items);
	t->items = v;
	t->size = newsize;
	return 1;
}

static inline int
insert_key_str(sickt *t, sickval k, sickval v)
{
	int accesses = SICKTABLE_MAX_ACCESS_RESIZE;
	struct sickti *item, **p;

	p = &t->items[AS_STRING(k)->hash % t->size];
	while (*p != NULL) {
		accesses--;
		if (sickval_equal(k, (*p)->key)) {
#if SICKT_DEBUG_WARN_OVERRIDE
			fprintf(stderr, "Overriding the value for the key ");
			sickval_print(stderr, k);
			fprintf(stderr, "\n");
#endif
			(*p)->val = v;
			return 1;
		} else
			p = &(*p)->next;
	}

	if ((item = calloc(1, sizeof(struct sickti))) == NULL)
		return 0;
	item->key = k;
	item->val = v;
	item->next = NULL;

	*p = item;

	if (accesses < 0)
		return sickt_resize(t);
	return 1;
}

int
sickt_insert(sickt *t, sickval k, sickval v)
{
	if (t == NULL)
		return 0;

	if (k.type != SV_STRING && k.type != SV_SYMBOL)
		return 0;

	return insert_key_str(t, k, v);
}

void
sickt_do(sickt *t, sickt_mapfn fn, void *data)
{
	size_t		i;
	struct sickti	*e;

	for (i = 0; i < t->size; ++i) {
		e = t->items[i];
		while (e != NULL) {
			fn(e->key, e->val, data);
			e = e->next;
		}
	}
}

void
sickt_dump(FILE *f, sickt *t)
{
	size_t i;
	struct sickti *j;

	fprintf(f, "==================\n");
	for (i = 0; i < t->size; ++i) {
		fprintf(f, "> ");
		j = t->items[i];
		while (j != NULL) {
			sickval_print(f, j->key);
			fprintf(f, ":");
			sickval_print(f, j->val);
			fprintf(f, " -> ");
			j = j->next;
		}
		fprintf(f, "NULL\n");
	}
	fprintf(f, "==================\n");
}

int
sickt_has(sickt *t, sickval k)
{
	return sickt_get(t, k) != NULL;
}

sickval *
sickt_get(sickt *t, sickval k)
{
	struct sickti *p;

	if (t == NULL)
		return NULL;

	p = t->items[AS_STRING(k)->hash % t->size];
	while (p != NULL) {
		if (sickval_equal(k, p->key))
			return &p->val;
		p = p->next;
	}

	return NULL;
}
