#ifndef SICKTABLE_H
#define SICKTABLE_H

#include <stddef.h>
#include <stdio.h>

#include "values.h"

struct sickti {
	sickval		key;
	sickval		val;
	struct sickti	*next;
};

typedef struct sickt {
	struct gcobject	gc;
	size_t		size;
	struct sickti	**items;
} sickt;

typedef void (*sickt_mapfn)(sickval, sickval, void*);

sickt	*sickt_new();
int	 sickt_init();
void	 sickt_deallocate(sickt*);
void	 sickt_destroy(sickt*);
size_t	 sickt_hash(const char*, ssize_t);
int	 sickt_insert(sickt*, sickval, sickval);
void	 sickt_do(sickt*, sickt_mapfn, void*);
void	 sickt_dump(FILE*, sickt*);
int	 sickt_has(sickt*, sickval);
sickval	*sickt_get(sickt*, sickval);

#endif
