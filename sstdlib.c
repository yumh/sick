#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "sstdlib.h"
#include "vm.h"
#include "compiler.h"
#include "util.h"

#define DEFUN_NUM(fnname, op)						\
	sickval								\
	fnname (sickvm *vm, struct vec *v)				\
	{								\
		sicknum res;						\
		sickval *f;						\
		int i;							\
		UNUSED(vm);						\
		assert(vec_cap(v) > 0);					\
		f = vec_nth(v, 0);					\
		S_ASSERT_TYPE(*f, SV_NUMBER);				\
		res = f->as.number;					\
		i = 0;							\
		VEC_FOREACH(sickval, n, v, {				\
				if (i != 0) {				\
					S_ASSERT_TYPE(n, SV_NUMBER);	\
					res op n.as.number;		\
				}					\
				++i;					\
			});						\
		return SICK_NUMBER(res);				\
	}

DEFUN_NUM(sstd_add, +=)
DEFUN_NUM(sstd_sub, -=)
DEFUN_NUM(sstd_mul, *=)
DEFUN_NUM(sstd_div, /=)

#undef DEFUN_NUM

sickval
sstd_eq(sickvm *vm, struct vec *v)
{
	sickval *f, *c;
	size_t i, j;

	UNUSED(vm);

	i = vec_len(v);
	if (i < 2)
		return SICK_TRUE;

	f = vec_nth(v, 0);
	for (j = 1; j < i; ++j) {
		c = vec_nth(v, j);
		if (!sickval_equal(*f, *c))
			return SICK_FALSE;
	}

	return SICK_TRUE;
}

#define DEFUN_CMPB(fnname, op)						\
	sickval								\
	fnname (sickvm *vm, struct vec *v)				\
	{								\
		sickval *a, *b;						\
		UNUSED(vm);						\
		if (vec_len(v) != 2)					\
			abort();					\
		a = vec_nth(v, 0);					\
		b = vec_nth(v, 1);					\
		if (a->type != SV_NUMBER || b->type != SV_NUMBER)	\
			abort();					\
		return SICK_BOOL(a->as.number op b->as.number);		\
	}								\

DEFUN_CMPB(sstd_gt, >);
DEFUN_CMPB(sstd_lt, <);
DEFUN_CMPB(sstd_ge, >=);
DEFUN_CMPB(sstd_le, <=);

#undef DEFUN_CMPB

sickval
sstd_not(sickvm *vm, struct vec *v)
{
	sickval *b;

	UNUSED(vm);

	if (vec_len(v) != 1)
		abort();

	b = vec_nth(v, 0);
	if (b->type == SV_NIL)
		return SICK_TRUE;
	return SICK_FALSE;
}

sickval
sstd_print(sickvm *vm, struct vec *v)
{
	sickval t;
	size_t i;
	int f;

	UNUSED(vm);

	f = 0;
	for (i = 0; i < v->count; ++i) {
		if (f)
			putchar(' ');
		t = ((sickval*)v->items)[i];
		sickval_print(stdout, t);
		f = 1;
	}

	/* VEC_FOREACH(sickval, i, v, { */
	/* 		if (f) */
	/* 			putchar(' '); */
	/* 		sickval_print(stdout, i); */
	/* 		f = 1; */
	/* 	}); */

	return SICK_NIL;
}

sickval
sstd_println(sickvm *vm, struct vec *v)
{
	sstd_print(vm, v);
	putchar('\n');
	return SICK_NIL;
}

sickval
sstd_flush(sickvm *vm, struct vec *v)
{
	UNUSED(vm);
	UNUSED(v);

	fflush(stdout);
	return SICK_NIL;
}

sickval
sstd_print_vars(sickvm *vm, struct vec *v)
{
	struct scope *s;

	UNUSED(v);

	s = SICKVM_ACTIVEFRAME(vm)->scope;
	while (s != NULL) {
		sickval_print(stdout, SICK_TABLE(s->scope));
		fprintf(stdout, "\n");
		s = s->parent;
	}

	return SICK_NIL;
}

sickval
sstd_read(sickvm *vm, struct vec *v)
{
	size_t len, read, c;
	char *line, *tmp;
	sickval s;

	UNUSED(v);

	len = 8;
	read = 0;
	line = calloc(len, sizeof(char));

	while ((c = getchar()) != EOF) {
		if (c == '\n')
			break;

		line[read++] = c;

		if (read >= len) {
			len += 8;

			tmp = realloc(line, len);
			if (tmp == NULL) {
				free(line);
				return SICK_NIL;
			}
			line = tmp;
		}
	}

	s = sickvm_newstr(vm, line, read);
	free(line);
	return s;
}

sickval
sstd_eval(sickvm *vm, struct vec *v)
{
	sickval ret = SICK_NIL;

	VEC_FOREACH(sickval, i, v, {
			if (i.type != SV_STRING)
				abort();

			/* TODO: namespace hardcoded! */
			if (!compile_and_eval_in_ns(
				    vm,
				    AS_STRING(i)->str,
				    AS_STRING(i)->len,
				    sickvm_newsym(vm, "user", -1),
				    &ret))
				abort();
		});

	return ret;
}

sickval
sstd_vec(sickvm *vm, struct vec *v)
{
	struct vec *vec;
	vec = sickvm_newvec(vm);

	VEC_FOREACH(sickval, i, v, {
			vec_push(vec, &i);
		});

	return SICK_VEC(vec);
}

sickval
sstd_map(sickvm *vm, struct vec *v)
{
	sickval t, *vec_end, *vec_cur;
	t = sickvm_newtable(vm);

	vec_cur = v->items;
	vec_end = (sickval*)v->items + v->count;

	assert (v->count % 2 == 0);

	for (; vec_cur < vec_end; vec_cur += 2) {
		sickt_insert(AS_TABLE(t), *vec_cur, *(vec_cur+1));
	}

	return t;
}

sickval
sstd_nth(sickvm *vm, struct vec *v)
{
	sickval *n, *l;

	UNUSED(vm);

	if (vec_len(v) != 2)
		abort();

	n = vec_nth(v, 0);
	l = vec_nth(v, 1);

	return *(sickval*)vec_nth(AS_VEC(*l), AS_NUMBER(*n));
}

void
sstd_load(sickvm *vm)
{
	/*
	 * TODO: create a namespace "sick.core" and load there the
	 * functions
	*/
	struct fn_defs {
		char *name;
		sickfn fn;
	};

	struct fn_defs fns[] = {
		{"print", sstd_print},
		{"println", sstd_println},
		{"flush", sstd_flush},
		{"+", sstd_add},
		{"-", sstd_sub},
		{"/", sstd_div},
		{"*", sstd_mul},
		{"=", sstd_eq},
		{"not", sstd_not},
		{"print-vars", sstd_print_vars},
		{"read", sstd_read},
		{"eval", sstd_eval},
		{"vec", sstd_vec},
		{"map", sstd_map},
		{"nth", sstd_nth},
		{">",  sstd_gt},
		{"<",  sstd_lt},
		{">=", sstd_ge},
		{"<=", sstd_le},
	};

	/* struct chunk *c; */
	struct frame *f;
	size_t i;

	if ((f = sickvm_newframe(vm, NULL, NULL)) == NULL)
		abort();

	sickt_insert(vm->namespaces, sickvm_newsym(vm, "sick.core", -1), SICK_FRAME(f));

	for (i = 0; i < sizeof(fns)/sizeof(struct fn_defs); ++i)
		sickt_insert(f->scope->scope, sickvm_newsym(vm, fns[i].name, -1), sickvm_newcfun(vm, fns[i].fn));

	/* load also some common vars */
	sickt_insert(f->scope->scope, sickvm_newsym(vm, "nil", -1), SICK_NIL);
	sickt_insert(f->scope->scope, sickvm_newsym(vm, "true", -1), SICK_TRUE);
	sickt_insert(f->scope->scope, sickvm_newsym(vm, "false", -1), SICK_FALSE);
}
