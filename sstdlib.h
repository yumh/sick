#ifndef SSTDLIB_H
#define SSTDLIB_H

#include "values.h"
#include "vm.h"

#define S_ASSERT_TYPE(v, t) \
	assert ((v).type == t);

/* Sick standard library! yay! */

sickval	sstd_add(struct sickvm*, struct vec*);
sickval	sstd_sub(struct sickvm*, struct vec*);
sickval	sstd_mul(struct sickvm*, struct vec*);
sickval	sstd_div(struct sickvm*, struct vec*);
sickval	sstd_eq(struct sickvm*, struct vec*);

sickval	sstd_print(struct sickvm*, struct vec*);
sickval	sstd_println(struct sickvm*, struct vec*);

void	sstd_load(struct sickvm*);

#endif
