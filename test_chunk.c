#include <stdlib.h>

#include "chunk.h"
#include "values.h"
#include "vec.h"

#define LOOPS 1024
#define BUFLEN 256

int
main(void)
{
	uint32_t buf[BUFLEN];
	struct chunk c;
	struct vec v;
	int i;

	vec_init(&v, sizeof(int));
	chunk_init(&c, &v);

	for (i = 0; i < LOOPS; ++i)
		if (chunk_write(&c, 1) == -1)
			abort();

	for (i = 0; i < LOOPS*2; ++i)
		if (chunk_addconst(&c, SICK_NUMBER(i)) == -1)
			abort();

	for (i = 0; i < BUFLEN; i++)
		buf[i] = i;

	if (!chunk_writebuf(&c, buf, BUFLEN))
		abort();

	chunk_deallocate(&c);

	return 0;
}
