#include <assert.h>
#include <string.h>
#include <stdio.h>

#include "compiler.h"
#include "vm.h"

#define NEW_TOKEN_NUMBER(n) ((struct token){.type = TOKEN_NUMBER, .start = n, .len = strlen(n), .line = 1})
#define READ_NUMBER(s) ((token_to_sickval(NULL, NEW_TOKEN_NUMBER(s))).as.number)

int
main(void)
{
	/* ATM test only the token_to_number function */
	assert(5 == READ_NUMBER("5"));
	assert(12 == READ_NUMBER("12"));
	assert(-5 == READ_NUMBER("-5"));
	assert(5.4 == READ_NUMBER("5.4"));
}
