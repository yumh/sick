#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "vm.h"
#include "util.h"

#define N_TABLES 256

sickval
new_table(sickvm *vm, struct vec *v)
{
	UNUSED(v);

	/* printf("creating a new table\n"); */
	return sickvm_newtable(vm);
}

int
main(void)
{
	sickvm *vm;

	if ((vm = sickvm_new()) == NULL)
		abort();

	/* ATM this is emtpy. When we will have a loop/recur construct this test will be populated */

	sickvm_destroy(vm);

	return 0;
}
