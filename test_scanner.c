#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "scanner.h"

#define RUN_TEST(prog, ...)						\
	{								\
		enum token_type e[] = { __VA_ARGS__, TOKEN_EOF };	\
		struct scanner s;					\
		int len, i;						\
		s = scanner_new(prog, strlen(prog));			\
		len = sizeof(e)/sizeof(enum token_type);		\
		for (i = 0; i < len; ++i) {				\
			struct token t;					\
			t = scanner_next_token(&s);			\
			if (e[i] != t.type) {				\
				printf("expecting a %s but got a %s\n",	\
				       scanner_token_str(e[i]),		\
				       scanner_token_str(t.type));	\
				abort();				\
			}						\
		}							\
	}

int
main(void)
{
	RUN_TEST("hello 99 0xFf (yup \"strings\")",
		 TOKEN_SYMBOL,
		 TOKEN_NUMBER, TOKEN_NUMBER,
		 TOKEN_LEFT_PAREN, TOKEN_SYMBOL,
		 TOKEN_STRING, TOKEN_RIGHT_PAREN);

	RUN_TEST("\"hellooo", TOKEN_UNTERMINATED_STRING);

	RUN_TEST("\'yoo", TOKEN_APOSTROPHE, TOKEN_SYMBOL);

	RUN_TEST(":yoo", TOKEN_COLON, TOKEN_SYMBOL);

	RUN_TEST(":yoo ; comments!", TOKEN_COLON, TOKEN_SYMBOL);

	RUN_TEST("[ a vector ]", TOKEN_LEFT_BRACKET, TOKEN_SYMBOL, TOKEN_SYMBOL, TOKEN_RIGHT_BRACKET);
	RUN_TEST("[a vector]", TOKEN_LEFT_BRACKET, TOKEN_SYMBOL, TOKEN_SYMBOL, TOKEN_RIGHT_BRACKET);
	RUN_TEST("(something something)", TOKEN_LEFT_PAREN, TOKEN_SYMBOL, TOKEN_SYMBOL, TOKEN_RIGHT_PAREN);

	RUN_TEST("{a map [with \"vectors\"]}", TOKEN_LEFT_BRACE, TOKEN_SYMBOL, TOKEN_SYMBOL, TOKEN_LEFT_BRACKET, TOKEN_SYMBOL, TOKEN_STRING, TOKEN_RIGHT_BRACKET, TOKEN_RIGHT_BRACE);

	RUN_TEST("{:a map}", TOKEN_LEFT_BRACE,TOKEN_COLON, TOKEN_SYMBOL, TOKEN_SYMBOL, TOKEN_RIGHT_BRACE);

	/* RUN_TEST("-5", TOKEN_NUMBER); */

	return 0;
}
