#!/bin/sh

n=${3:-200}
seq=`$1 $n`

{
    echo "#include <stdio.h>"
    echo "#include <stdlib.h>"
    echo "#include <string.h>"
    echo "#include \"sicktable.h\""
    echo "#include \"values.h\""
    echo "#include \"util.h\""

    echo "void dump_table(sickval k, sickval v, void *data) {"
    echo " UNUSED(data);"
    echo " sickval_print(stdout, k);"
    echo " printf(\": \");"
    echo " sickval_print(stdout, v);"
    echo " printf(\"\\\n\");"
    echo "}"

    echo "int main() {"
    echo " sickt *t;"
    echo " sickval v;"
    for i in $seq; do
	echo " struct sick_str s$i;"
	echo " sickval k$i;"
    done

    echo " if ((t = sickt_new()) == NULL)"
    echo "  abort();"

    echo " v = SICK_NIL;"

    for i in $seq; do
	str="key$i"
	echo " s$i.hash = sickt_hash(\"$str\", -1);"
	echo " s$i.len = strlen(\"$str\");"
	echo " s$i.str = \"$str\";"
	echo " k$i.type = SV_STRING;"
	echo " k$i.as.gcobject = &s$i;";
	echo " if (!sickt_insert(t, k$i, v))"
	echo "  abort();"
    done

    echo " sickt_do(t, &dump_table, NULL);"
    echo " sickt_destroy(t);"
    echo " return 0;"
    echo "}"
} > $2
