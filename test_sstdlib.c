#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "vm.h"
#include "sstdlib.h"

int
main(void)
{
	/*
	 * TEST TEMPRORARY DISABLED
	 *
	 * after the arrive of `loop' in the stdlib this test will be
	 * written entirely in sicklisp
	 */

	/* struct vec v; */
	/* sickvm *vm; */

	/* vec_init(&v, sizeof(sickval)); */
	/* vec_resize(&v, 2); */
	/* vec_push(&v, &SICK_NUMBER(2)); */
	/* vec_push(&v, &SICK_NUMBER(8)); */

	/* if ((vm = sickvm_new()) == NULL) */
	/* 	abort(); */

	/* assert(sstd_add(vm, &v).as.number == 10); */
	/* assert(sstd_mul(vm, &v).as.number == 16); */
	/* assert(sstd_sub(vm, &v).as.number == -6); */
	/* assert(sstd_div(vm, &v).as.number == 0.25); */

	/* assert(sstd_eq(vm, &v).type == SICK_FALSE.type); */
	/* vec_pop(&v); */
	/* vec_push(&v, &SICK_NUMBER(2)); */
	/* assert(sstd_eq(vm, &v).type != SICK_TRUE.type); */

	/* sickvm_destroy(vm); */
	/* vec_deallocate(&v); */

	return 0;
}
