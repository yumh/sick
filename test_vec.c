#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "vec.h"
#include "util.h"

#define SIZE 100

struct _test_ {
	int foo;
	char bar;
	long buzz;
};

void
dump_vec(void *a, void *data)
{
	struct _test_ i = *(struct _test_*)a;
	UNUSED(data);
	printf("%d\n", i.foo);
}

int
main(void)
{
	struct vec	*v;
	int		i;
	struct _test_	t;

	if ((v = vec_new(sizeof(struct _test_))) == NULL)
		abort();

	for (i = 0; i < SIZE; ++i) {
		t.foo = t.bar = t.buzz = i;
		if (vec_push(v, &t) < 0)
			abort();
	}

	assert(vec_len(v) == SIZE);

	i = -1;
	VEC_FOREACH(struct _test_, j, v, {
			assert(i+1 == j.foo);
			i++;
		});

	i = 1;
	assert(!memcmp(&i, vec_nth(v, 1), sizeof(int)));

	for (i = 0; i < SIZE/2; ++i)
		vec_delete(v, i);
	assert(vec_len(v) == SIZE/2);

	vec_do(v, dump_vec, NULL);

	for (i = 0; i < SIZE; ++i)
		vec_pop(v);
	assert(vec_len(v) == 0);

	vec_destroy(v);
	return 0;
}
