#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "vm.h"
#include "util.h"

int
main(void)
{
	sickvm *vm;

	if ((vm = sickvm_new()) == NULL)
		abort();

	/* gc should be paused by default */
	assert(vm->gcpaused);

	sickvm_gc_resume(vm);
	assert(!vm->gcpaused);

	sickvm_gc_pause(vm);
	assert(vm->gcpaused);

	sickvm_destroy(vm);
}
