#include <stdlib.h>
#include <string.h>

#include "util.h"

void *
memdup(const void *src, size_t len)
{
	void *p;

	if ((p = malloc(len)) == NULL)
		return NULL;

	memcpy(p, src, len);
	return p;
}
