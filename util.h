#ifndef UTIL_H
#define UTIL_H

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))

#define UNUSED(...) (void)(__VA_ARGS__)

#include <stddef.h>

/*
 * if we're on C89 define inline and restrict as macro so the code
 * compiles. GCC/Clang with -std=c89 does not complain, so it's good?
 */
#if __STD_VERSION__ < 199901L
# ifndef inline
#  define inline
# endif
# ifndef restrict
#  define restrict
# endif
#endif

void	*memdup(const void *, size_t);

#endif
