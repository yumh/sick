#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sicktable.h"
#include "values.h"
#include "vec.h"

int
sickval_equal(sickval x, sickval y)
{
	if (x.type != y.type)
		return 0;

	switch (x.type) {
	case SV_NIL:
		return 1;

	case SV_BOOL:
		return x.as.bool == y.as.bool;

	case SV_SYMBOL:
	case SV_STRING:
		if (AS_STRING(x)->len != AS_STRING(y)->len)
			return 0;
		if (AS_STRING(x)->hash != AS_STRING(y)->hash)
			return 0;
		return !strncmp(AS_STRING(x)->str, AS_STRING(y)->str, AS_STRING(x)->len);

	case SV_NUMBER:
		return x.as.number == y.as.number;

	case SV_TABLE:
		/* FIXME: change this */
		return 0;

	default:
		return 0;
	}
}

static void
print_table(sickval k, sickval v, void *data)
{
	FILE *f = data;

	sickval_print(f, k);
	fprintf(f, " ");
	sickval_print(f, v);
	fprintf(f, " ");
}

void
sickval_print(FILE *f, sickval v)
{
	switch (v.type) {
	case SV_NIL:
		fprintf(f, "nil");
		return;

	case SV_BOOL:
		fprintf(f, "%s", v.as.bool ? "true" : "false");
		return;

	case SV_STRING:
		fprintf(f, "%.*s", (int)AS_STRING(v)->len, AS_STRING(v)->str);
		return;

	case SV_SYMBOL:
		fprintf(f, ":%.*s", (int)AS_SYMBOL(v)->len, AS_SYMBOL(v)->str);
		return;

	case SV_NUMBER:
		fprintf(f, SICK_NUM_TYPE_F, v.as.number);
		return;

	case SV_TABLE:
		fprintf(f, "{");
		sickt_do(AS_TABLE(v), print_table, f);
		fprintf(f, "}");
		return;

	case SV_VEC: {
		int j = 0;

		fprintf(f, "[");
		VEC_FOREACH(sickval, i, AS_VEC(v), {
				j++;
				sickval_print(f, i);
				fprintf(f, " ");
			});
		if (j)
			fputc('\b', f);
		fprintf(f, "]");
		return;
	}

	case SV_FRAME:
		fprintf(f, "<frame>");
		return;

	case SV_FUN:
		fprintf(f, AS_FUN(v)->cfn ? "<internal fn>" : "<fn>");
		return;

	case SV_CHUNK:
		fprintf(f, "<chunk>");
		return;

	case SV_SCOPE:
		fprintf(f, "<scope>");
		return;

	default:
		fprintf(f, "Don't know how to print an object of type %d\n", v.type);
		abort();
	}
}
