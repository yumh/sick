#ifndef VALUES_H
#define VALUES_H

#include <stddef.h>
#include <stdio.h>

#ifndef SICK_NUM_TYPE
# define SICK_NUM_TYPE double
#endif

#ifndef SICK_NUM_TYPE_F
# define SICK_NUM_TYPE_F "%f"
#endif

#define SICK_NIL	((sickval){ .type = SV_NIL })
#define SICK_NUMBER(s)	((sickval){ .type = SV_NUMBER, .as = { .number = s } })
#define SICK_BOOL(b)	((sickval){ .type = SV_BOOL, .as = { .bool = b }})
#define SICK_TRUE	SICK_BOOL(1)
#define SICK_FALSE	SICK_BOOL(0)
#define SICK_STRING(s)	((sickval){ .type = SV_STRING, .as = { .gcobject = s }})
#define SICK_SYMBOL(s)	((sickval){ .type = SV_SYMBOL, .as = { .gcobject = s }})
#define SICK_TABLE(t)	((sickval){ .type = SV_TABLE, .as = { .gcobject = t }})
#define SICK_FUN(t)	((sickval){ .type = SV_FUN, .as = { .gcobject = t }})
#define SICK_FRAME(t)	((sickval){ .type = SV_FRAME, .as = { .gcobject = t }})
#define SICK_CHUNK(t)	((sickval){ .type = SV_CHUNK, .as = { .gcobject = t } })
#define SICK_VEC(t)	((sickval){ .type = SV_VEC, .as = { .gcobject = t } })
#define SICK_SCOPE(t)	((sickval){ .type = SV_SCOPE, .as = { .gcobject = t } })

#define AS_NUMBER(v)	((sicknum)(v).as.number)
#define AS_TABLE(v)	((sickt*)(v).as.gcobject)
#define AS_FUN(v)	((struct sick_fun*)(v).as.gcobject)
#define AS_STRING(v)	((struct sick_str*)(v).as.gcobject)
#define AS_SYMBOL(v)	((struct sick_str*)(v).as.gcobject)
#define AS_FRAME(v)	((struct frame*)(v).as.gcobject)
#define AS_CHUNK(v)	((struct chunk*)(v).as.gcobject)
#define AS_VEC(v)	((struct vec*)(v).as.gcobject)
#define AS_SCOPE(v)	((struct scope*)(v).as.gcobject)

#define TO_BOOL(v)	(((v).type == SV_BOOL && (v).as.bool) || ((v).type != SV_NIL && (v).type != SV_BOOL))

typedef SICK_NUM_TYPE sicknum;

enum sickval_type {
	SV_NIL,			/* 0 */
	SV_BOOL,		/* 1 */
	SV_NUMBER,		/* 2 */
	SV_STRING,		/* 3 */
	SV_SYMBOL,		/* 4 */
	SV_TABLE,		/* 5 */
	SV_FUN,			/* 6 */
	SV_CHUNK,		/* 7 */
	SV_VEC,			/* 8 */
	SV_FRAME,		/* 9 */
	SV_SCOPE,		/* 10 */
};

/* declare structure defined elsewhere */
struct sickval;
struct sickvm;
struct sickt;
struct frame;
struct vec;

struct gcobject {
	int		marked;
	int		type;
	struct gcobject	*next;
};

typedef struct sickval (*sickfn)(struct sickvm*, struct vec*);
struct sick_fun {
	struct gcobject	gc;
	int		cfn;
	union {
		sickfn		cfun;
		struct {
			struct frame	*frame;
			unsigned int	arity;
		} nativefn;
	} fn;
	/* NOTE: will be used to provide overloaded variants of this function */
	/* struct sick_fun	*next; */
};

struct sick_str {
	struct gcobject	gc;
	size_t	hash;
	size_t	len;
	char	*str;
};

typedef struct sickval {
	int type;
	union {
		sicknum		number;
		int		bool;
		void		*gcobject;
	} as;
} sickval;

int	sickval_equal(sickval, sickval);
void	sickval_print(FILE*, sickval);

#endif
