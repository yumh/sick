#include <stdlib.h>
#include <string.h>

#include "vec.h"
#include "util.h"

/* TODO: fill with zero when reallocating! */

struct vec *
vec_new(size_t size)
{
	struct vec	*v;

	if ((v = calloc(1, sizeof(struct vec))) == NULL)
		return NULL;

	vec_init(v, size);

	return v;
}

void
vec_init(struct vec *v, size_t size)
{
	v->items = NULL;
	v->size = size;
	v->capacity = 0;
	v->count = 0;
}

void
vec_destroy(struct vec *v)
{
	vec_deallocate(v);
	free(v);
}

void
vec_deallocate(struct vec *v)
{
	free(v->items);
}

size_t
vec_len(struct vec *v)
{
	return v->count;
}

size_t
vec_cap(struct vec *v)
{
	return v->capacity;
}

int
vec_push(struct vec *v, void *i)
{
	void	*p;
	size_t	newsize;

	if (i == NULL)
		abort();

	if (v->capacity == v->count) {
		newsize = VEC_GROW(v->capacity);
		p = realloc(v->items, newsize * v->size);
		if (p == NULL)
			return -1;
		v->capacity = newsize;
		v->items = p;
	}

	memcpy(v->items + v->count * v->size, i, v->size);
	return v->count++;
}

void *
vec_pop(struct vec *v)
{
	if (v->count == 0)
		return NULL;

	return v->items + (v->count--) * v->size;
}

void *
vec_nth(struct vec *v, size_t n)
{
	/* Technically should be v->count, but... */
	if (v->capacity < n)
		return NULL;
	return v->items + n * v->size;
}

int
vec_set(struct vec *v, size_t n, void *d)
{
	if (d == NULL)
		abort();

	if (v->capacity < n) {
		fprintf(stderr, "Cannot set %zu, it's way beyond my max %zu\n", n, v->capacity);
		abort();
		/* return 0; */
	}

	memcpy(v->items + n * v->size, d, v->size);
	return 1;
}

void
vec_delete(struct vec *v, size_t n)
{
	if (v->count < n)
		return;

	if (v->count - 1 < n)
		memcpy(v->items + n * v->size,
		       v->items + (n+1) * v->size,
		       (v->count - n) * v->size);
	v->count--;
}

void
vec_do(struct vec *c, void (*fn)(void*, void*), void *data)
{
	void *i;
	for (i = c->items; i < c->items + (c->count * c->size); i += c->size)
		fn(i, data);
}

int
vec_resize(struct vec *v, size_t newcap)
{
	void *p;

	if ((p = realloc(v->items, newcap * v->size)) == NULL)
		return 0;

	v->items = p;
	v->capacity = newcap;
	v->count = MIN(v->count, newcap);
	return 1;
}
