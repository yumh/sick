#ifndef VEC_H
#define VEC_H

#include <stddef.h>

#include "values.h"

#define VEC_GROW(cap) \
	((cap) < 8 ? 8 : ((cap) + ((cap) >> 1)))

#define VEC_FOREACH(type, i, v, c)					\
	{								\
		void *vec_end;						\
		void *vec_cur;						\
		vec_cur = (v)->items;					\
		vec_end = (v)->items + ((v)->count * (v)->size);	\
		for (; vec_cur < vec_end; vec_cur += (v)->size) {	\
			type i = *(type*)vec_cur;			\
			c;						\
		}							\
	}

struct vec {
	struct gcobject	gc;
	void		*items;
	size_t		count;
	size_t		capacity;
	size_t		size;
};

struct vec	*vec_new(size_t);
void		 vec_init(struct vec*, size_t);
void		 vec_deallocate(struct vec*);
void		 vec_destroy(struct vec*);
size_t		 vec_len(struct vec*);
size_t		 vec_cap(struct vec*);
int		 vec_push(struct vec*, void*);
void		*vec_pop(struct vec*);
void		*vec_nth(struct vec*, size_t);
int		 vec_set(struct vec*, size_t, void*);
void		 vec_delete(struct vec*, size_t);
void		 vec_do(struct vec*, void (*)(void*, void*), void*);
int		 vec_resize(struct vec*, size_t);

#endif
