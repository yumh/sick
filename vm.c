/* remove */
#include <stdio.h>

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "sllimits.h"
#include "sstdlib.h"
#include "vm.h"
#include "util.h"

#include "compiler.h"
#include "debug.h"

#define VM_NEXT_I(i, f) ((i) += (f).code->bytecode.size)

sickvm *
sickvm_new()
{
	sickvm *vm;

	if ((vm = calloc(1, sizeof(sickvm))) == NULL)
		return NULL;

	vm->gcpaused = 1;
	vm->objects = NULL;
	vm->maxobj = SICK_INITIAL_GC_THRESHOLD;

	vm->strings = sickvm_newtable(vm).as.gcobject;

	vm->stack_top = vm->stack;
	vm->stack_end = vm->stack_end + SICK_MAX_STACK;

	vm->namespaces = AS_TABLE(sickvm_newtable(vm));

	sstd_load(vm);

	return vm;
}

void
sickvm_destroy(sickvm *vm)
{
	struct gcobject *o, *n;

	o = vm->objects;
	while (o != NULL) {
		n = o->next;
		sickvm_delete(o);
		o = n;
	}

	free(vm);
}

static inline void
sweep(sickvm *vm)
{
	struct gcobject **o, *unreachable;

	o = &vm->objects;
	while (*o != NULL) {
		if ((*o)->marked) {
			(*o)->marked = 0;
			o = &(*o)->next;
		} else {
			unreachable = *o;
			*o = unreachable->next;
			sickvm_delete(unreachable);
			vm->nobj--;
		}
	}
}

static inline void mark(sickval v);

static void
mark_table(sickval k, sickval v, void *data)
{
	UNUSED(data);

	mark(k);
	mark(v);
}

static void
mark_vec(void *p, void *data)
{
	sickval *v = p;
	UNUSED(data);
	mark(*v);
}

static inline void
mark(sickval v)
{
	switch (v.type) {
	case SV_NIL:
	case SV_NUMBER:
	case SV_BOOL:
		break;

	case SV_SYMBOL:
	case SV_STRING:
		AS_STRING(v)->gc.marked = 1;
		break;

	case SV_FUN:
		if (AS_FUN(v)->gc.marked)
			break;
		AS_FUN(v)->gc.marked = 1;

		if (!AS_FUN(v)->cfn)
			mark(SICK_FRAME(AS_FUN(v)->fn.nativefn.frame));
		break;

	case SV_TABLE:
		/* if (AS_TABLE(v)->gc.marked) */
		/* 	break; */
		AS_TABLE(v)->gc.marked = 1;
		sickt_do(AS_TABLE(v), mark_table, NULL);
		break;

	case SV_CHUNK:
		/* if (AS_CHUNK(v)->gc.marked) */
		/* 	break; */
		AS_CHUNK(v)->gc.marked = 1;
		mark(SICK_VEC(AS_CHUNK(v)->cp));
		vec_do(AS_CHUNK(v)->cp, mark_vec, NULL);
		break;

	case SV_VEC:
		/* if (AS_VEC(v)->gc.marked) */
		/* 	break; */
		AS_VEC(v)->gc.marked = 1;
		vec_do(AS_VEC(v), mark_vec, NULL);
		break;

	case SV_FRAME:
		/* sometimes i change only the vec, so we ALWAYS need to mark recursively */
		/* if (AS_FRAME(v)->gc.marked) */
		/* 	break; */
		AS_FRAME(v)->gc.marked = 1;

		if (AS_FRAME(v)->chunk != NULL)
			mark(SICK_CHUNK(AS_FRAME(v)->chunk));

		if (AS_FRAME(v)->reg != NULL)
			mark(SICK_VEC(AS_FRAME(v)->reg));

		if (AS_FRAME(v)->scope != NULL)
			mark(SICK_SCOPE(AS_FRAME(v)->scope));
		break;

	case SV_SCOPE:
		/* if (AS_SCOPE(v)->gc.marked) */
		/* 	break; */
		AS_SCOPE(v)->gc.marked = 1;

		if (AS_SCOPE(v)->scope != NULL)
			mark(SICK_TABLE(AS_SCOPE(v)->scope));

		if (AS_SCOPE(v)->parent != NULL)
			mark(SICK_SCOPE(AS_SCOPE(v)->parent));
		break;

	default:
		fprintf(stderr, "Don't know how to mark an object with type: %d\n", v.type);
		abort();
	}
}

static inline void
markall(sickvm *vm)
{
	struct frame	*fp;

	vm->namespaces->gc.marked = 1;
	sickt_do(vm->namespaces, mark_table, NULL);

	vm->strings->gc.marked = 1;
	sickt_do(vm->strings, mark_table, NULL);

	for (fp = vm->stack; fp <= vm->stack_top; ++fp)
		mark(SICK_FRAME(fp));
}

void
sickvm_gc(sickvm *vm)
{
	if (vm->gcpaused) {
		/* printf("GC wanted to run (%zu), but it's paused!\n", vm->nobj); */
		return;
	}

#if VM_GC_PRINT_STATS
	printf(">> gc kiks in with %zu objects <<\n", vm->nobj);
#endif
	markall(vm);
	sweep(vm);

	vm->maxobj = MAX(SICK_INITIAL_GC_THRESHOLD, vm->nobj * 1.5);
#if VM_GC_PRINT_STATS
	printf("\tnew max = %zu (objects %zu)\n", vm->maxobj, vm->nobj);
#endif
}

void
sickvm_gc_pause(sickvm *vm)
{
	vm->gcpaused = 1;
}

void
sickvm_gc_resume(sickvm *vm)
{
	vm->gcpaused = 0;
	if (vm->maxobj <= vm->nobj)
		sickvm_gc(vm);
}

static inline void
define(struct frame * restrict current, sickop * restrict i)
{
	sickval *s, *v;
	s = vec_nth(current->reg, i->u.op1);
	v = vec_nth(current->reg, i->u.op2);
	sickt_insert(current->scope->scope, *s, *v);
}

static inline void
fetch(sickvm *restrict vm, struct frame *restrict current, sickop *restrict i)
{
	sickval *s;
	s = chunk_constnth(current->chunk, i->u.op1);

	if (i->u.op3) {
		sickval fn;
		int l;

		assert(s->type == SV_FUN && !AS_FUN(*s)->cfn);
		assert(AS_FUN(*s)->fn.nativefn.frame->scope == NULL);

		l = AS_FUN(*s)->fn.nativefn.frame->reg->capacity;

		fn = sickvm_clonefn(vm, AS_FUN(*s), current->scope);
		s = &fn;

		vec_resize(AS_FUN(fn)->fn.nativefn.frame->reg, l);
	}

	vec_set(current->reg, i->u.op2, s);
}

static inline void
load(struct frame *restrict current, sickop * restrict i)
{
	sickval *v, *k;
	struct scope *ff;

	k = chunk_constnth(current->chunk, i->u.op1);
	if (k->type != SV_SYMBOL)
		abort();
	v = NULL;
	ff = current->scope;
	while (ff != NULL) {
		/* printf(">>> in loop\n"); */
		if ((v = sickt_get(ff->scope, *k)) != NULL)
			break;
		/* sickval_print(stderr, SICK_TABLE(ff->scope)); */
		ff = ff->parent;
	}
	if (v == NULL) {
		fprintf(stderr, "Cannot get ");
		sickval_print(stderr, *k);
		fprintf(stderr, "\n");
		abort();
	}
	/* printf("<<< done\n"); */
	vec_set(current->reg, i->u.op2, v);

	/* printf("loaded :"); */
	/* sickval_print(stdout, *v); */
	/* printf("\n"); */
}

static inline void
call(sickvm *vm, struct frame **restrict current, sickop **restrict i, sickop **restrict end)
{
	sickval *fn, *r;
	int arity, vlen;

	fn = vec_nth((*current)->reg, (*i)->u.op1);
	if (fn->type != SV_FUN) {
		sickval_print(stderr, *fn);
		fprintf(stderr, " is not a function!\n");
		abort();
	}

	if (AS_FUN(*fn)->cfn) {
		r = vec_nth((*current)->reg, (*i)->u.op2);
		*r = AS_FUN(*fn)->fn.cfun(vm, vm->stack_top->reg);

		/* discard the frame */
		vm->stack_top->reg = NULL;
		vm->stack_top->scope = NULL;
		vm->stack_top->chunk = NULL;
		return;
	}

	(*current)->reg_ret = (*i)->u.op2;

	/* 0-arity functions have reg = NULL, fix that */
	if (vm->stack_top->reg == NULL) {
		vlen = 0;
		vm->stack_top->reg = sickvm_newvec(vm);
	} else
		vlen = vec_len(vm->stack_top->reg);

	arity = AS_FUN(*fn)->fn.nativefn.arity;

	/* check arity */
	if (arity != vlen) {
		fprintf(stderr, "Function called with the wrong arity! (%d vs %d)\n", vlen, arity);
		abort();
	}

	vm->stack_top->chunk = AS_FUN(*fn)->fn.nativefn.frame->chunk;
	vm->stack_top->scope = sickvm_newscope(vm, AS_FUN(*fn)->fn.nativefn.frame->scope);
	vec_resize(vm->stack_top->reg, AS_FUN(*fn)->fn.nativefn.frame->reg->capacity);

	/* save the current state and CALL */
	(*current)->pc = (*i);
	(*current)->reg_ret = (*i)->u.op2;
	(*current) = vm->stack_top++;

	/* -1 because at the next loop will be incrementd */
	(*i) = (*current)->chunk->bytecode.items;
	(*i)--;
	(*end) = (*i) + (*current)->chunk->bytecode.count + 1;
}

static inline sickval
run(sickvm *vm)
{
	sickop	*i, *e;
	struct frame *current, *base;

	if (vm->stack_top == vm->stack) {
		fprintf(stderr, "Load a frame before calling run()\n");
		abort();
	}

	sickvm_gc_resume(vm);

	current = SICKVM_ACTIVEFRAME(vm);
	base = current;

	i = current->chunk->bytecode.items;
	e = i + current->chunk->bytecode.count;
	for (; i < e; i++) {

#if VM_DEBUG_INSTRUCTIONS
		fprintf(stderr, "Executing:\t");
		compile_dump_instruction(current->chunk, *i, stdout);
#endif

		switch (i->u.i) {
		case OP_DEFINE:
			define(current, i);
			break;

		case OP_FETCH:
			WITH_GC_PAUSED(vm, {
					fetch(vm, current, i);
				});
			break;

		case OP_LOAD:
			load(current, i);
			break;

		case OP_PUSH: {
			assert(vm->stack_top == (current + 1));

			if (vm->stack_top == vm->stack_end)
				abort();

			if (vm->stack_top->reg == NULL)
				vm->stack_top->reg = sickvm_newvec(vm);

			vec_push(vm->stack_top->reg, vec_nth(current->reg, i->u.op1));
			break;
		}

		case OP_CALL:
			if (vm->stack_top == vm->stack_end)
				abort();

			WITH_GC_PAUSED(vm, {
					call(vm, &current, &i, &e);
				});
			break;

		case OP_RET: {
			sickval ret;

			ret = *(sickval*)vec_nth(current->reg, i->u.op1);

			/* discard the current frame */
			current->reg = NULL;
			current->scope = NULL;
			current->chunk = NULL;

			/* TODO: Watch for underflow */
			vm->stack_top--;

			if (current == base)
				goto ret;

			/* otherwise pop the frame and continue */
			current--;
			i = current->pc;
			e = ((sickop*)current->chunk->bytecode.items) + current->chunk->bytecode.count;
			vec_set(current->reg, current->reg_ret, &ret);
			break;
		}

		case OP_TEST: {
			sickval *t;
			t = vec_nth(current->reg, i->u.op1);

			/* If (*t) is true-ish, skip the next instruction */
			++i;
			if (TO_BOOL(*t))
				break;
			/* Otherwise the next instruction MUST BE a JMP, so execute it directly */

		} /* FALLTHROUGH */

		case OP_JMP:
			i += i->ls.op1;

			/*
			 * i can be equal to the start of the bytecode
			 * - 1, because on the next iteration will be
			 * incremented
			 */
			if (i < ((sickop*)current->chunk->bytecode.items - 1) || i > e) {
				fprintf(stderr, "JMP to a invalid position\n");
				abort();
			}
			break;

		case OP_COPY:
			vec_set(current->reg, i->u.op2, vec_nth(current->reg, i->u.op1));
			break;

			/* General OP -- I suck at naming things */
#define GOP(r) {							\
				sickval *a, *b, c;			\
				a = vec_nth(current->reg, i->u.op1);	\
				b = vec_nth(current->reg, i->u.op2);	\
				assert(a->type == SV_NUMBER && b->type == a->type); \
				c = r;\
				vec_set(current->reg, i->u.op3, &c);	\
			}

		case OP_ADD:
			GOP(SICK_NUMBER(AS_NUMBER(*a) + AS_NUMBER(*b)));
			break;

		case OP_SUB:
			GOP(SICK_NUMBER(AS_NUMBER(*a) - AS_NUMBER(*b)));
			break;

		case OP_MUL:
			GOP(SICK_NUMBER(AS_NUMBER(*a) * AS_NUMBER(*b)));
			break;

		case OP_DIV:
			GOP(SICK_NUMBER(AS_NUMBER(*a) / AS_NUMBER(*b)));
			break;

		case OP_EQ:
			GOP(SICK_BOOL(sickval_equal(*a, *b)));
			break;

		case OP_NOT: {
			sickval *a, b;
			a = vec_nth(current->reg, i->u.op1);
			b = SICK_BOOL(!TO_BOOL(*a));
			vec_set(current->reg, i->u.op2, &b);
			break;
		}

#undef G_OP_A

		default:
			fprintf(stderr, "Unknown opcode %d\n", i->u.i);
			abort();
			break;
		}
	}

	/* printf("chunk finished!\n"); */

ret:
	if (vec_cap(current->reg) > 0)
		return *(sickval*)vec_nth(current->reg, 0);
	else
		return SICK_NIL;
}

sickval
sickvm_run(sickvm *vm)
{
	sickval r;

	vm->gcpaused = 0;
	r = run(vm);
	vm->gcpaused = 1;
	return r;
}

sickval
sickvm_runchunk(sickvm *vm, struct chunk *c, size_t reg_no)
{
	struct frame *f, *parent;
	sickval *parent_frame;

	parent_frame = sickt_get(vm->namespaces, sickvm_newsym(vm, "sick.core", -1));
	parent = parent_frame == NULL ? NULL : AS_FRAME(*parent_frame);

	if ((f = sickvm_newframe(vm, parent, c)) == NULL)
		return SICK_NIL;

	/* s.fp = f; */
	vec_resize(f->reg, reg_no);
	return sickvm_push(vm, f);
}

/* Eval a frame it only check the  in the current scope */
sickval
sickvm_eval(sickvm *vm, struct frame *s)
{
	struct frame *current;

	current = SICKVM_ACTIVEFRAME(vm);

	if (current == NULL)
		abort();

	s->scope = current->scope;

	/* Just make sure it terminates */
	/* chunk_write(s->code, SICKOP_RET(0)); */

	/* then run it */
	return sickvm_push(vm, s);
}

sickval
sickvm_eval_in_ns(sickvm *vm, struct frame *s, sickval ns)
{
	sickval *n;

	if (ns.type != SV_SYMBOL)
		abort();

	n = sickt_get(vm->namespaces, ns);
	if (n == NULL) {
		fprintf(stderr, "Creating the namespace ");
		sickval_print(stderr, ns);
		fprintf(stderr, "\n");

		sickt_insert(vm->namespaces, ns, SICK_FRAME(s));
		n = sickt_get(vm->namespaces, ns);
	}

	s->scope = AS_FRAME(*n)->scope;

	return sickvm_push(vm, s);
}

sickval
sickvm_push(sickvm *vm, struct frame *s)
{
	if (vm == NULL)
		return SICK_NIL;

	/* TODO: check for frame stack overflow! */
	*(vm->stack_top++) = *s;

	return sickvm_run(vm);
}

void *
sickvm_newgcobj(sickvm *vm, enum sickval_type t, size_t s)
{
	struct gcobject *g;

	assert(s > sizeof(struct gcobject));

	if (vm->nobj >= vm->maxobj)
		sickvm_gc(vm);

	if ((g = calloc(1, s)) == NULL)
		return NULL;

	g->type = t;
	g->next = vm->objects;
	vm->objects = g;
	vm->nobj++;

	return g;
}

struct scope *
sickvm_newscope(sickvm *vm, struct scope *parent)
{
	struct scope *s;

	if ((s = sickvm_newgcobj(vm, SV_SCOPE, sizeof(struct scope))) == NULL)
		abort();

	s->parent = parent;
	s->scope = AS_TABLE(sickvm_newtable(vm));
	return s;
}

struct frame *
sickvm_newframe(sickvm *vm, struct frame *parent, struct chunk *c)
{
	struct frame *f;

	if ((f = sickvm_newgcobj(vm, SV_FRAME, sizeof(struct frame))) == NULL)
		abort();

	f->scope = sickvm_newscope(vm, parent == NULL ? NULL : parent->scope);
	f->chunk = c;
	f->reg = sickvm_newvec(vm);

	return f;
}

struct frame *
sickvm_newframe_light(sickvm *vm, struct chunk *c)
{
	struct frame *f;

	if ((f = sickvm_newgcobj(vm, SV_FRAME, sizeof(struct frame))) == NULL)
		abort();
	f->scope = NULL;
	f->chunk = c;
	f->reg = sickvm_newvec(vm);
	return f;
}

static inline sickval
new_stringish(sickvm *vm, const char *s, int len, enum sickval_type type)
{
	struct sick_str *v;
	sickval ret;

	if ((v = sickvm_newgcobj(vm, type, sizeof(struct sick_str))) == NULL)
		abort();

	v->len  = len == -1 ? strlen(s) : len;
	v->hash = sickt_hash(s, v->len);

	/* TODO: insert the string in vm->strings to reduce space */
	v->str = memdup(s, v->len);
	if (v->str == NULL)
		abort();

	ret.type = type;

	ret.as.gcobject = v;
	return ret;
}

sickval
sickvm_newstr(sickvm *vm, const char *s, ssize_t len)
{
	return new_stringish(vm, s, len, SV_STRING);
}

sickval
sickvm_newsym(sickvm *vm, const char *s, ssize_t len)
{
	return new_stringish(vm, s, len, SV_SYMBOL);
}

sickval
sickvm_newtable(sickvm *vm)
{
	sickt *t;

	if ((t = sickvm_newgcobj(vm, SV_TABLE, sizeof(sickt))) == NULL)
		abort();

	if (!sickt_init(t))
		abort();

	return SICK_TABLE(t);
}

struct chunk *
sickvm_newchunk(sickvm *vm)
{
	struct chunk *c;
	struct vec *cp;

	if ((c = sickvm_newgcobj(vm, SV_CHUNK, sizeof(struct chunk))) == NULL)
		abort();

	if ((cp = sickvm_newvec(vm)) == NULL)
		abort();

	chunk_init(c, cp);

	return c;
}

struct chunk *
sickvm_newchunk_inherit(sickvm *vm, struct chunk *cc)
{
	struct chunk *c;

	if ((c = sickvm_newgcobj(vm, SV_CHUNK, sizeof(struct chunk))) == NULL)
		abort();
	chunk_init(c, cc->cp);
	return c;
}

struct vec *
sickvm_newvec(sickvm *vm)
{
	struct vec *v;

	if ((v = sickvm_newgcobj(vm, SV_VEC, sizeof(struct vec))) == NULL)
		abort();

	vec_init(v, sizeof(sickval));
	return v;
}

sickval
sickvm_newcfun(sickvm *vm, sickfn fn)
{
	struct sick_fun *f;

	if ((f = sickvm_newgcobj(vm, SV_FUN, sizeof(struct sick_fun))) == NULL)
		abort();

	f->cfn = 1;
	f->fn.cfun = fn;
	/* f->next = NULL; */
	return SICK_FUN(f);
}

sickval
sickvm_newfn(sickvm *vm, struct frame *f, int arity)
{
	struct sick_fun *fn;

	if ((fn = sickvm_newgcobj(vm, SV_FUN, sizeof(struct sick_fun))) == NULL)
		abort();

	fn->cfn = 0;
	fn->fn.nativefn.frame = f;
	fn->fn.nativefn.arity = arity;

	/* fn->next = NULL; */

	return SICK_FUN(fn);
}

sickval
sickvm_clonefn(sickvm *vm, struct sick_fun *f, struct scope *parent)
{
	struct sick_fun *clone;

	if ((clone = sickvm_newgcobj(vm, SV_FUN, sizeof(struct sick_fun))) == NULL)
		abort();

	clone->cfn = 0;
	clone->fn.nativefn.arity = f->fn.nativefn.arity;
	clone->fn.nativefn.frame = sickvm_newframe(vm, NULL, f->fn.nativefn.frame->chunk);
	clone->fn.nativefn.frame->scope = sickvm_newscope(vm, parent);

	return SICK_FUN(clone);
}

void
sickvm_delete_val(sickval v)
{
	switch (v.type) {
	case SV_NIL:
	case SV_NUMBER:
		break;

	default:
		/* the struct gcobject is the first field of the pointer in v.as */
		sickvm_delete((struct gcobject*)v.as.gcobject);
		break;
	}
}

void
sickvm_delete(struct gcobject *g)
{
	switch (g->type) {
	case SV_TABLE:
		sickt_destroy((sickt*)g);
		break;

	case SV_SYMBOL:
	case SV_STRING: {
		struct sick_str *s = (void*)g;
		free(s->str);
		free(s);
		break;
	}

	case SV_FRAME:
		free(g);
		break;

	case SV_FUN:
		free(g);
		break;

	case SV_VEC: {
		struct vec *v;
		v = (struct vec*)g;
		vec_destroy(v);
		break;
	}

	case SV_CHUNK:
		chunk_deallocate((struct chunk*)g);
		free(g);
		break;

	case SV_SCOPE:
		free(g);
		break;

	default:
		fprintf(stderr, "TODO: free this object of type %d\n", g->type);
		abort();
	}
}

#undef VM_NEXT_I
