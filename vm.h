#ifndef VM_H
#define VM_H

#include <stdint.h>

#include "chunk.h"
#include "sicktable.h"
#include "sllimits.h"

#define SICKOP_DEFINE(s, r)	(((sickop){.u = {.i=OP_DEFINE, .op1=s, .op2=r}}).raw)
#define SICKOP_FETCH(s, r)	(((sickop){.u = {.i=OP_FETCH, .op1=s, .op2=r, .op3=0}}).raw)
#define SICKOP_FINSCOPE(s, r)	(((sickop){.u = {.i=OP_FETCH, .op1=s, .op2=r, .op3=1}}).raw)
#define SICKOP_LOAD(s, r)	(((sickop){.u = {.i=OP_LOAD, .op1=s, .op2=r}}).raw)
#define SICKOP_PUSH(r)		(((sickop){.u = {.i=OP_PUSH, .op1=r}}).raw)
#define SICKOP_CALL(s, r)	(((sickop){.u = {.i=OP_CALL, .op1=s, .op2=r}}).raw)
#define SICKOP_RET(r)		(((sickop){.u = {.i=OP_RET, .op1=r}}).raw)
#define SICKOP_JMP(l)		(((sickop){.ls= {.i=OP_JMP, .op1=l}}).raw)
#define SICKOP_TEST(r)		(((sickop){.u = {.i=OP_TEST, .op1=r}}).raw)
#define SICKOP_COPY(j, k)	(((sickop){.u = {.i=OP_COPY, .op1=(j), .op2=(k)}}).raw)

#define SICKOP_ADD(a, b, c)	(((sickop){.u = {.i=OP_ADD, .op2=a, .op2=b, .op3=c}}).raw)
#define SICKOP_SUB(a, b, c)	(((sickop){.u = {.i=OP_SUB, .op2=a, .op2=b, .op3=c}}).raw)
#define SICKOP_MUL(a, b, c)	(((sickop){.u = {.i=OP_MUL, .op2=a, .op2=b, .op3=c}}).raw)
#define SICKOP_DIV(a, b, c)	(((sickop){.u = {.i=OP_DIV, .op2=a, .op2=b, .op3=c}}).raw)
#define SICKOP_EQ(a, b, c)	(((sickop){.u = {.i=OP_EQ, .op2=a, .op2=b, .op3=c}}).raw)
#define SICKOP_NOT(a, b)	(((sickop){.u = {.i=OP_NOT, .op2=a, .op2=b}}).raw)

#define SICKVM_ACTIVEFRAME(v)	((v)->stack_top == (v)->stack ? NULL : ((v)->stack_top)-1)

#define WITH_GC_PAUSED(vm, c)	{sickvm_gc_pause(vm); c; sickvm_gc_resume(vm);}

enum sickop {
	OP_DEFINE,
	OP_FETCH,
	OP_LOAD,
	OP_PUSH,
	OP_CALL,
	OP_RET,
	OP_TEST,
	OP_JMP,
	OP_COPY,

	OP_ADD,
	OP_SUB,
	OP_MUL,
	OP_DIV,
	OP_EQ,
	OP_NOT,
};

typedef union {
	uint32_t	raw;
	struct {
		uint8_t	i;
		uint8_t	op1;
		uint8_t	op2;
		uint8_t	op3;
	} u;
	struct {
		uint8_t	i;
		int8_t	op1;
		int8_t	op2;
		int8_t	op3;
	} s;
	struct {
		uint8_t	i;
		uint8_t	padding;
		uint16_t op1;
	} lu;
	struct {
		uint8_t i;
		uint8_t padding;
		int16_t op1;
	} ls;
} sickop;

struct scope {
	struct gcobject	gc;
	struct scope	*parent;
	sickt		*scope;
};

struct frame {
	struct gcobject	gc;
	struct scope	*scope;
	struct chunk	*chunk;
	struct vec	*reg;
	uint8_t		reg_ret;
	sickop		*pc;
};

typedef struct sickvm {
	struct frame	stack[SICK_MAX_STACK];
	struct frame	*stack_end;
	struct frame	*stack_top;
	struct gcobject	*objects;
	size_t		nobj;
	size_t		maxobj;
	int		gcpaused;
	sickt		*strings;
	sickt		*namespaces;
} sickvm;

sickvm		*sickvm_new();
void		 sickvm_destroy(sickvm*);
void		 sickvm_gc(sickvm*);

void		 sickvm_gc_pause(sickvm*);
void		 sickvm_gc_resume(sickvm*);

sickval		 sickvm_run(sickvm*);
sickval		 sickvm_runchunk(sickvm*, struct chunk *c, size_t);
sickval		 sickvm_eval(sickvm*, struct frame*);
sickval		 sickvm_eval_in_ns(sickvm*, struct frame*, sickval);
sickval		 sickvm_push(sickvm*, struct frame*);

void		*sickvm_newgcobj(sickvm*, enum sickval_type, size_t);
struct scope	*sickvm_newscope(sickvm*, struct scope*);
struct frame	*sickvm_newframe(sickvm*, struct frame*, struct chunk*);
struct frame	*sickvm_newframe_light(sickvm*, struct chunk*);
sickval		 sickvm_newstr(sickvm*, const char*, ssize_t);
sickval		 sickvm_newsym(sickvm*, const char*, ssize_t);
sickval		 sickvm_newtable(sickvm*);
struct chunk	*sickvm_newchunk(sickvm*);
struct chunk	*sickvm_newchunk_inherit(sickvm*, struct chunk*);
struct vec	*sickvm_newvec(sickvm*);
sickval		 sickvm_newcfun(sickvm*, sickfn);
sickval		 sickvm_newfn(sickvm*, struct frame*, int);
sickval		 sickvm_clonefn(sickvm*, struct sick_fun*, struct scope*);
void		 sickvm_delete_val(sickval);
void		 sickvm_delete(struct gcobject*);

#endif
